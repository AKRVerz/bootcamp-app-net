import React from 'react';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import { Container } from '@mui/material';
import MainResetPasswordPage from '../../BaseComponents/ComponentsLogin/MainResetPasswordPage';

const ResetPasswordPage = () => {
  return (
    <div>
      <Header showButtons={true} />
      <Container>
        <MainResetPasswordPage />
      </Container>
    </div>
  );
};

export default ResetPasswordPage;
