import React from "react";
import HeaderAdmin from "../../BaseComponents/AdminComponent/HeaderAdmin";
import UserComponent from "../../BaseComponents/AdminComponent/UserComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const AdminUserPage = () => {
  return (
    <div>
      <HeaderAdmin showButtons={true} />
      <Sidebar>
        <UserComponent />
      </Sidebar>
    </div>
  );
};

export default AdminUserPage;
