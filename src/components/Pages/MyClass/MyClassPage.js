import FooterComponent from '../../BaseComponents/ComponentsHome/FooterComponent';

import React, { useContext, useEffect, useState } from 'react';

import { Container } from '@mui/system';
import ListCarComponent from '../../BaseComponents/ComponentClass/ListCarComponent';
import HeaderHome from '../../BaseComponents/ComponentsHome/HeaderHome';
import CardCarComponent from '../../BaseComponents/ComponentsHome/CardCarComponent';
import { AuthContext } from '../../../contexts/AuthContext';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import { CssBaseline } from '@mui/material';

const MyClassPage = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const authContext = useContext(AuthContext); // Get the auth context

  // Update isLoggedIn based on the auth context
  useEffect(() => {
    setIsLoggedIn(authContext.isLoggedIn);
  }, [authContext]);
  return (
    <div>
      <CssBaseline />
      {isLoggedIn ? (
        <HeaderHome showButtons={true} onLogout={authContext.logout} /> // Use authContext.logout for logout
      ) : (
        <Header showButtons={true} onLogin={authContext.login} /> // Use authContext.login for login
      )}
      <Container>
        <ListCarComponent />
        <CardCarComponent title="Another Favorite Course" />
        <FooterComponent />
      </Container>
    </div>
  );
};

export default MyClassPage;
