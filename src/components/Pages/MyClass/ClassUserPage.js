import FooterComponent from '../../BaseComponents/ComponentsHome/FooterComponent';
import React, { useContext, useEffect, useState } from 'react';
import { Box, Container, CssBaseline } from '@mui/material';
import HeaderHome from '../../BaseComponents/ComponentsHome/HeaderHome';
import { AuthContext } from '../../../contexts/AuthContext';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import ClassUser from '../../BaseComponents/ComponentClass/ClassUser';

const ClassUserPage = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const authContext = useContext(AuthContext); // Dapatkan konteks otentikasi

  // Perbarui isLoggedIn berdasarkan konteks otentikasi
  useEffect(() => {
    setIsLoggedIn(authContext.isLoggedIn);
  }, [authContext]);

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh', // Pastikan halaman minimal setinggi viewport
      }}
    >
      <CssBaseline />
      <HeaderHome showButtons={true} />
      <Container sx={{ flex: 1 }}><ClassUser /></Container>
      <Box sx={{ flexShrink: 0 }}>
        <FooterComponent />
      </Box>
    </Box>
  );
};

export default ClassUserPage;
