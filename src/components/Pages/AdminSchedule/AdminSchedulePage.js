import React from 'react';
import Sidebar from '../../BaseComponents/AdminComponent/SidebarAdmin';
import ScheduleComponent from '../../BaseComponents/AdminComponent/ScheduleComponent';
const AdminSchedulePage = () => {
  return (
    <div>
      <Sidebar>
        <ScheduleComponent />
      </Sidebar>
    </div>
  );
};

export default AdminSchedulePage;
