import React from "react";
import CourseComponent from "../../BaseComponents/AdminComponent/CourseComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const AdminCoursePage = () => {
  return (
    <div>
      <Sidebar>
        <CourseComponent />
      </Sidebar>
    </div>
  );
};

export default AdminCoursePage;
