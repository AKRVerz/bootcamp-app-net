import React from 'react';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import { Container } from 'react-bootstrap';
import MainPaymentSuccess from '../../BaseComponents/ComponentSuccessPayment/MainPaymentSuccess';

const SuccessPaymentPage = () => {
  return (
    <div>
      <Container>
        <MainPaymentSuccess />
      </Container>
    </div>
  );
};

export default SuccessPaymentPage;
