import React from 'react';
import { Container } from '@mui/material';
import MainConfirmEmailPage from '../../BaseComponents/ComponentsLogin/MainConfirmEmailPage';
import Header from '../../BaseComponents/ComponentsLogin/Header';

const ConfirmEmailPage = () => {
  return (
    <div>
      <Header showButtons={false} />
      <Container>
        <MainConfirmEmailPage />
      </Container>
    </div>
  );
};

export default ConfirmEmailPage;
