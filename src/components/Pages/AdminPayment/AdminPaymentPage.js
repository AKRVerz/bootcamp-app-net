import React from "react";
import PaymentComponent from "../../BaseComponents/AdminComponent/PaymentComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const AdminPaymentPage = () => {
  return (
    <div>
      <Sidebar>
        <PaymentComponent />
      </Sidebar>
    </div>
  );
};

export default AdminPaymentPage;
