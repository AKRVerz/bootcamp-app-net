import React, { useContext, useEffect, useState } from 'react';
import { Container } from '@mui/material';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import MainCheckOutPage from '../../BaseComponents/ComponentCheckOut/MainCheckOutPage';
import { AuthContext } from '../../../contexts/AuthContext';
import { CssBaseline } from '@mui/material';
import HeaderHome from '../../BaseComponents/ComponentsHome/HeaderHome';

const CheckOutPage = () => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const authContext = useContext(AuthContext); // Get the auth context

    // Update isLoggedIn based on the auth context
    useEffect(() => {
      setIsLoggedIn(authContext.isLoggedIn);
    }, [authContext]);
  return (
    <div>
      <CssBaseline />
      {isLoggedIn ? (
        <HeaderHome showButtons={true} onLogout={authContext.logout} /> // Use authContext.logout for logout
      ) : (
        <Header showButtons={false} onLogin={authContext.login} /> // Use authContext.login for login
      )}
      <Container>
        <MainCheckOutPage />
      </Container>
    </div>
  );
};

export default CheckOutPage;
