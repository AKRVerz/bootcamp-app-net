import React from 'react';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import { Container } from '@mui/material';
import MainNewPasswordPage from '../../BaseComponents/ComponentsLogin/MainNewPasswordPage';
const NewPasswordPage = () => {
  return (
    <div>
      <Header showButtons={true} />
      <Container>
        <MainNewPasswordPage />
      </Container>
    </div>
  );
};

export default NewPasswordPage;
