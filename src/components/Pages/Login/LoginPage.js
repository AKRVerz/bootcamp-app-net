import React from 'react';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import { Container } from '@mui/material';
import MainLoginPage from '../../BaseComponents/ComponentsLogin/MainLoginPage';

const LoginPage = () => {
  return (
    <div>
      <Header showButtons={true} />
      <Container>
        <MainLoginPage />
      </Container>
    </div>
  );
};

export default LoginPage;
