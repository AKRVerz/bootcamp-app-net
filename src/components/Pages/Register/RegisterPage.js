import React from 'react';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import { Container } from '@mui/material';
import MainRegisterPage from '../../BaseComponents/ComponentsLogin/MainRegisterPage';

const RegisterPage = () => {
  return (
    <div>
      <Header showButtons={true} />
      <Container>
        <MainRegisterPage />
      </Container>
    </div>
  );
};

export default RegisterPage;
