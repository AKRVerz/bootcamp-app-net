import React from 'react';
import Sidebar from '../../BaseComponents/AdminComponent/SidebarAdmin';
import CreateScheduleComponent from '../../BaseComponents/AdminAddComponent/CreateScheduleComponent';

const CreateSchedulePage = () => {
  return (
    <div>
      <Sidebar>
        <CreateScheduleComponent />
      </Sidebar>
    </div>
  );
};

export default CreateSchedulePage;
