import React from "react";
import { Container } from "@mui/material";
import HeaderAdmin from "../../BaseComponents/AdminComponent/HeaderAdmin";
import CreateUserComponent from "../../BaseComponents/AdminAddComponent/CreateUserComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const CreateUserPage = () => {
  return (
    <div>
      <Sidebar>
        <CreateUserComponent />
      </Sidebar>
    </div>
  );
};

export default CreateUserPage;
