import React from "react";
import CreateCourseComponent from "../../BaseComponents/AdminAddComponent/CreateCourseComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const CreateCoursePage = () => {
  return (
    <div>
      <Sidebar>
        <CreateCourseComponent />
      </Sidebar>
    </div>
  );
};

export default CreateCoursePage;
