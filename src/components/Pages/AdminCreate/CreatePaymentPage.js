import React from "react";
import { Container } from "@mui/material";
import CreatePaymentComponent from "../../BaseComponents/AdminAddComponent/CreatePaymentComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const CreatePaymentPage = () => {
  return (
    <div>
      <Sidebar>
        <CreatePaymentComponent />
      </Sidebar>
    </div>
  );
};

export default CreatePaymentPage;
