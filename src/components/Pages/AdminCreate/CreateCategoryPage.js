import React from "react";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";
import CreateCategoryComponent from "../../BaseComponents/AdminAddComponent/CreateCategoryComponent";

const CreateCategoryPage = () => {
  return (
    <div>
      <Sidebar>
        <CreateCategoryComponent />
      </Sidebar>
    </div>
  );
};

export default CreateCategoryPage;
