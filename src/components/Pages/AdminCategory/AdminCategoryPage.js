import React from "react";
import CategoryComponent from "../../BaseComponents/AdminComponent/CategoryComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";
const AdminCategoryPage = () => {
  return (
    <div>
      <Sidebar>
        <CategoryComponent />
      </Sidebar>
    </div>
  );
};

export default AdminCategoryPage;
