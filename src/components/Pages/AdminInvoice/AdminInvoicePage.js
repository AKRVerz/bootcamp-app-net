import React from "react";
import InvoiceComponent from "../../BaseComponents/AdminComponent/InvoiceComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const AdminInvoicePage = () => {
  return (
    <div>
      <Sidebar>
        <InvoiceComponent />
      </Sidebar>
    </div>
  );
};

export default AdminInvoicePage;
