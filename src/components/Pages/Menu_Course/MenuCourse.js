import React, { useContext, useEffect, useState } from 'react';
import { CssBaseline, Container } from '@mui/material';
import FooterComponent from '../../BaseComponents/ComponentsHome/FooterComponent';
import BanerMenuCourse from '../../BaseComponents/ComponentsHome/BanerMenuCourse';
import CardCarComponent from '../../BaseComponents/ComponentsHome/CardCarComponent';
import HeaderHome from '../../BaseComponents/ComponentsHome/HeaderHome';
import { AuthContext } from '../../../contexts/AuthContext';
import Header from '../../BaseComponents/ComponentsLogin/Header';

const MenuCourse = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const authContext = useContext(AuthContext); // Get the auth context

  // Update isLoggedIn based on the auth context
  useEffect(() => {
    setIsLoggedIn(authContext.isLoggedIn);
  }, [authContext]);
  return (
    <div>
      <CssBaseline />
      {isLoggedIn ? (
        <HeaderHome showButtons={true} onLogout={authContext.logout} /> // Use authContext.logout for logout
      ) : (
        <Header showButtons={true} onLogin={authContext.login} /> // Use authContext.login for login
      )}
      <BanerMenuCourse />
      <Container>
        <CardCarComponent title="Another Favorite Course" />
        <FooterComponent />
      </Container>
    </div>
  );
};

export default MenuCourse;
