import React, { useContext, useEffect, useState } from 'react';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import { Container } from 'react-bootstrap';
import FooterComponent from '../../BaseComponents/ComponentsHome/FooterComponent';
import MainInvoice from '../../BaseComponents/ComponentInvoice/MainInvoice';
import { AuthContext } from '../../../contexts/AuthContext';
import HeaderHome from '../../BaseComponents/ComponentsHome/HeaderHome';

const InvoicePage = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const authContext = useContext(AuthContext); // Get the auth context

  // Update isLoggedIn based on the auth context
  useEffect(() => {
    setIsLoggedIn(authContext.isLoggedIn);
  }, [authContext]);
  return (
    <div
      style={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}
    >
      {isLoggedIn ? (
        <HeaderHome showButtons={true} onLogout={authContext.logout} />
      ) : (
        <Header showButtons={true} onLogin={authContext.login} />
      )}
      <Container style={{ flex: '1' }}>
        <MainInvoice />
      </Container>
      <FooterComponent />
    </div>
  );
};

export default InvoicePage;
