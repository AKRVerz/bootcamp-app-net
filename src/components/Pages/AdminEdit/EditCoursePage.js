import React from "react";
import { Container } from "@mui/material";
import HeaderAdmin from "../../BaseComponents/AdminComponent/HeaderAdmin";
import EditCourseComponent from "../../BaseComponents/EditAdminComponent/EditCourseComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const EditCoursePage = () => {
  return (
    <div>
      <Sidebar>
        <EditCourseComponent />
      </Sidebar>
    </div>
  );
};

export default EditCoursePage;
