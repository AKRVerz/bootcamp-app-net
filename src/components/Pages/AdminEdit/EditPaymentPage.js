import React from "react";
import EditPaymentComponent from "../../BaseComponents/EditAdminComponent/EditPaymentComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const EditPaymentPage = () => {
  return (
    <div>
      <Sidebar>
        <EditPaymentComponent />
      </Sidebar>
    </div>
  );
};

export default EditPaymentPage;
