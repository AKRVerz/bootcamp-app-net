import React from "react";
import EditCatgoryComponent from "../../BaseComponents/EditAdminComponent/EditCatgoryComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const EditCategoryPage = () => {
  return (
    <div>
      <Sidebar>
        <EditCatgoryComponent />
      </Sidebar>
    </div>
  );
};

export default EditCategoryPage;
