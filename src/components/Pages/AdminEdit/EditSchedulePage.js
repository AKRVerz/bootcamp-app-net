import React from 'react';
import Sidebar from '../../BaseComponents/AdminComponent/SidebarAdmin';
import EditScheduleComponent from '../../BaseComponents/EditAdminComponent/EditScheduleComponent';

const EditSchedulePage = () => {
  return (
    <div>
      <Sidebar>
        <EditScheduleComponent />
      </Sidebar>
    </div>
  );
};

export default EditSchedulePage;
