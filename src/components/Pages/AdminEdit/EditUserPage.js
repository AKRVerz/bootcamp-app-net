import React from "react";
import { Container } from "@mui/material";
import HeaderAdmin from "../../BaseComponents/AdminComponent/HeaderAdmin";
import EditUserComponent from "../../BaseComponents/EditAdminComponent/EditUserComponent";
import Sidebar from "../../BaseComponents/AdminComponent/SidebarAdmin";

const EditUserPage = () => {
  return (
    <div>
      <Sidebar>
        <EditUserComponent />
      </Sidebar>
    </div>
  );
};

export default EditUserPage;
