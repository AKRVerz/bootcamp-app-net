import React, { useState, useContext, useEffect } from 'react';
import { CssBaseline, Container } from '@mui/material';
import HeaderHome from '../../BaseComponents/ComponentsHome/HeaderHome';
import BanerComponent from '../../BaseComponents/ComponentsHome/BanerComponent';
import CardCarComponent from '../../BaseComponents/ComponentsHome/CardCarComponent';
import BanefitComponents from '../../BaseComponents/ComponentsHome/BanefitComponents';
import FooterComponent from '../../BaseComponents/ComponentsHome/FooterComponent';
import { CartProvider } from '../../../contexts/CartContext';
import Header from '../../BaseComponents/ComponentsLogin/Header';
import { AuthContext } from '../../../contexts/AuthContext'; // Import AuthContext

const Home = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const authContext = useContext(AuthContext); // Get the auth context

  // Update isLoggedIn based on the auth context
  useEffect(() => {
    setIsLoggedIn(authContext.isLoggedIn);
  }, [authContext]);

  return (
    <div>
      <CssBaseline />
      <CartProvider>
        {isLoggedIn ? (
          <HeaderHome showButtons={true} onLogout={authContext.logout} /> // Use authContext.logout for logout
        ) : (
          <Header showButtons={true} onLogin={authContext.login} /> // Use authContext.login for login
        )}
        <BanerComponent />
        <Container>
          <CardCarComponent title="Join us for the course" />
          <BanefitComponents />
          <FooterComponent />
        </Container>
      </CartProvider>
    </div>
  );
};

export default Home;
