import React from 'react';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Portal from '@mui/material/Portal';

const Popup = ({ open, message, onClose, isSuccess }) => {
  const popupStyle = {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    background: 'rgba(0, 0, 0, 0.5)',
    zIndex: 9999,
  };

  const contentStyle = {
    background: 'white',
    padding: '20px',
    borderRadius: '4px',
    boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2)',
  };

  const buttonStyle = {
    marginTop: '10px',
  };

  return (
    <Portal>
      {open && (
        <div style={popupStyle}>
          <div style={contentStyle}>
            <Typography variant="body1">{message}</Typography>
            <Button variant="contained" onClick={onClose} style={buttonStyle}>
              Close
            </Button>
          </div>
        </div>
      )}
    </Portal>
  );
};

export default Popup;
