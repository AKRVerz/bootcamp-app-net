import React, { useState, useEffect } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import axios from 'axios';

const PaymentComponent = () => {
  const [blogs, setBlogs] = useState([]);
  const apiUrl = process.env.REACT_APP_API_URL;

  useEffect(() => {
    fetch(`${apiUrl}/api/PaymentMethod`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        console.log(data);
        setBlogs(data);
      });
  }, []);

  const handleActivate = (id) => {
    // Mengirim PUT request ke API untuk mengaktifkan metode pembayaran dengan ID tertentu
    axios
      .put(`${apiUrl}/api/PaymentMethod/updateActiveCourse/${id}`)
      .then((response) => {
        console.log('Metode pembayaran diaktifkan:', response.data);
        // Refresh daftar metode pembayaran setelah mengaktifkannya
        fetchData();
      })
      .catch((error) => {
        console.error('Error activating payment method:', error);
      });
  };

  const handleDeactivate = (id) => {
    // Mengirim PUT request ke API untuk menonaktifkan metode pembayaran dengan ID tertentu
    axios
      .put(
        `${apiUrl}/api/PaymentMethod/updateNotActiveCourse/${id}`
      )
      .then((response) => {
        console.log('Metode pembayaran dinonaktifkan:', response.data);
        // Refresh daftar metode pembayaran setelah menonaktifkannya
        fetchData();
      })
      .catch((error) => {
        console.error('Error deactivating payment method:', error);
      });
  };

  const fetchData = () => {
    // Memperbarui daftar metode pembayaran setelah mengubah status
    fetch(`${apiUrl}/api/PaymentMethod`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setBlogs(data);
      });
  };

  return (
    <div>
      <Link to="/adminCreatePayment">
        <Button
          variant="contained"
          color="primary"
          style={{ marginBottom: '16px' }}
        >
          Tambah Payment Method
        </Button>
      </Link>
      <TableContainer component={Paper}>
        <Table sx={{ pb: 15, minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Name</TableCell>
              
              <TableCell align="center">Status</TableCell>
              <TableCell align="center">Action</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {blogs.map((row) => (
              <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell align="center" component="th" scope="row">
                  {row.name}
                </TableCell>
                
                <TableCell align="center">
                  {row.status === 1 ? 'Aktif' : 'Tidak Aktif'}
                </TableCell>
                <TableCell align="center">
                  <Link to={`/adminEditPayment/${row.id}`}>
                    <Button variant="contained">Edit</Button>
                  </Link>
                </TableCell>
                <TableCell align="center">
                  {row.status === 1 ? (
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => handleDeactivate(row.id)}
                    >
                      Non-Active
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => handleActivate(row.id)}
                    >
                      Active
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default PaymentComponent;
