import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import axios from 'axios';

const CourseComponent = () => {
  const [courses, setCourses] = useState([]);
const apiUrl = process.env.REACT_APP_API_URL;
  useEffect(() => {
    fetchCourses();
  }, []);

  const fetchCourses = () => {
    fetch(`${apiUrl}/api/Course`)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setCourses(data);
      });
  };

  const handleActivate = (id) => {
    axios
      .put(`${apiUrl}/api/Course/updateActiveCourse/${id}`)
      .then((response) => {
        console.log('Kursus diaktifkan:', response.data);
        fetchCourses();
      })
      .catch((error) => {
        console.error('Error activating course:', error);
      });
  };

  const handleDeactivate = (id) => {
    // Mengirim PUT request ke API untuk menonaktifkan kursus dengan ID tertentu
    axios
      .put(`${apiUrl}/api/Course/updateNotActiveCourse/${id}`)
      .then((response) => {
        console.log('Kursus dinonaktifkan:', response.data);
        // Refresh daftar kursus setelah menonaktifkannya
        fetchCourses();
      })
      .catch((error) => {
        console.error('Error deactivating course:', error);
      });
  };

  return (
    <div>
      <Link to="/adminCreateCourse">
        <Button
          variant="contained"
          color="primary"
          style={{ marginBottom: '16px' }}
        >
          Tambah Course
        </Button>
      </Link>
      <TableContainer component={Paper}>
        <Table sx={{ pb: 15, minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Title</TableCell>
              <TableCell align="center">Deskripsi</TableCell>
              <TableCell align="center">Harga</TableCell>
              <TableCell align="center">Status</TableCell>
              <TableCell align="center">Action</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {courses.map((row) => (
              <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell align="center" component="th" scope="row">
                  {row.title}
                </TableCell>
                <TableCell align="center">{row.description}</TableCell>
                <TableCell align="center">{row.price}</TableCell>
                <TableCell align="center">
                  {row.isActive === 1 ? 'Aktif' : 'Tidak Aktif'}
                </TableCell>
                <TableCell align="center">
                  <Link to={`/adminEditCourse/${row.id}`}>
                    <Button variant="contained">Edit</Button>
                  </Link>
                </TableCell>
                <TableCell align="center">
                  {row.isActive === 1 ? (
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => handleDeactivate(row.id)}
                    >
                      Nonaktifkan
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => handleActivate(row.id)}
                    >
                      Aktifkan
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default CourseComponent;
