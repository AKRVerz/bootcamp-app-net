import React, { useState, useEffect } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';
import axios from 'axios';

const CategoryComponent = () => {
  const [categories, setCategories] = useState([]);
const apiUrl = process.env.REACT_APP_API_URL;
  useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = () => {
    fetch(`${apiUrl}/api/Category`)
      .then((response) => response.json())
      .then((data) => setCategories(data));
  };

  const handleActivate = (id) => {
    // Mengirim PUT request ke API untuk mengaktifkan kategori dengan ID tertentu
    axios
      .put(`${apiUrl}/api/Category/putActive/${id}`)
      .then((response) => {
        console.log('Kategori diaktifkan:', response.data);
        // Refresh daftar kategori setelah mengaktifkannya
        fetchCategories();
      })
      .catch((error) => {
        console.error('Error activating category:', error);
      });
  };

  const handleDeactivate = (id) => {
    // Mengirim PUT request ke API untuk menonaktifkan kategori dengan ID tertentu
    axios
      .put(`${apiUrl}/api/Category/putNotActive/${id}`)
      .then((response) => {
        console.log('Kategori dinonaktifkan:', response.data);
        // Refresh daftar kategori setelah menonaktifkannya
        fetchCategories();
      })
      .catch((error) => {
        console.error('Error deactivating category:', error);
      });
  };

  return (
    <div>
      <Link to="/adminCreateCategory">
        <Button
          variant="contained"
          color="primary"
          style={{ marginBottom: '16px' }}
        >
          Tambah Category
        </Button>
      </Link>
      <TableContainer component={Paper}>
        <Table sx={{ pb: 15, minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Name</TableCell>
              <TableCell align="center">Deskripsi</TableCell>
              <TableCell align="center">Status</TableCell>
              <TableCell align="center">Action</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {categories.map((row) => (
              <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell align="center" component="th" scope="row">
                  {row.categoryName}
                </TableCell>
                <TableCell align="center">{row.deskripsi}</TableCell>
                <TableCell align="center">
                  {row.isActive === 1 ? 'Aktif' : 'Tidak Aktif'}
                </TableCell>
                <TableCell align="center">
                  <Link to={`/adminEditCategory/${row.id}`}>
                    <Button variant="contained">Edit</Button>
                  </Link>
                </TableCell>
                <TableCell align="center">
                  {row.isActive === 1 ? (
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => handleDeactivate(row.id)}
                    >
                      Non-Active
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => handleActivate(row.id)}
                    >
                      Active
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default CategoryComponent;
