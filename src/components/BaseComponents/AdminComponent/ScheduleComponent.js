import React, { useState, useEffect } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';

const ScheduleComponent = () => {
  const [schedules, setSchedules] = useState([]);
  const [courses, setCourses] = useState({});
  const apiUrl = process.env.REACT_APP_API_URL;

  useEffect(() => {
    // Mengambil data jadwal dari API saat komponen dimuat
    fetchScheduleData();
  }, []);

  const fetchScheduleData = () => {
    fetch(`${apiUrl}/api/Schedule`)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setSchedules(data);

        // Mengambil semua data course
        fetch(`${apiUrl}/api/Course`)
          .then((response) => response.json())
          .then((courseData) => {
            console.log(courseData);
            // Mengubah array course menjadi objek dengan courseId sebagai kunci
            const courseMap = courseData.reduce((acc, course) => {
              acc[course.id] = course;
              return acc;
            }, {});
            setCourses(courseMap);
          })
          .catch((error) => {
            console.error('Error fetching course data:', error);
          });
      })
      .catch((error) => {
        console.error('Error fetching schedule data:', error);
      });
  };

  return (
    <div>
      <h2>Schedule and Course Details</h2>
      <Link to="/adminCreateSchedule">
        <Button
          variant="contained"
          color="primary"
          style={{ marginBottom: '16px' }}
        >
          Tambah Schedule Method
        </Button>
      </Link>
      <TableContainer component={Paper}>
        <Table sx={{ pb: 15, minWidth: 650 }} aria-label="schedule table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Tanggal</TableCell>
              <TableCell align="center">Course ID</TableCell>
              <TableCell align="center">Title</TableCell>
              {/* Tambahkan kolom-kolom lain dari data course yang ingin Anda tampilkan */}
            </TableRow>
          </TableHead>
          <TableBody>
            {schedules.map((schedule) => (
              <TableRow
                key={schedule.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell align="center" component="th" scope="row">
                  {schedule.jadwal}
                </TableCell>
                <TableCell align="center">{schedule.courseFK}</TableCell>
                <TableCell align="center">
                  {courses[schedule.courseFK]
                    ? courses[schedule.courseFK].title
                    : ''}
                </TableCell>
                {/* <TableCell align="center">
                  <Link to={`/adminEditSchedule/${schedule.id}`}>
                    <Button variant="contained">Edit</Button>
                  </Link>
                </TableCell> */}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default ScheduleComponent;
