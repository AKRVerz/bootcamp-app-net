import React, { useState, useEffect } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import axios from 'axios';
import { Link } from 'react-router-dom';

const UserComponent = () => {
  const [users, setUsers] = useState([]);
  const apiUrl = process.env.REACT_APP_API_URL;

  // useEffect(() => {
  //   fetchUsers();
  // }, []);

  // const fetchUsers = () => {
  //   fetch('${apiUrl}/api/User')
  //     .then((response) => response.json())
  //     .then((data) => {
  //       console.log(data);
  //       setUsers(data);
  //     });
  // };
  useEffect(() => {
    fetch(`${apiUrl}/api/User`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data);
      setUsers(data);
    })
  }, []);
const fetchUsers = () => {
  // Memperbarui daftar metode pembayaran setelah mengubah status
  fetch(`${apiUrl}/api/User`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      setUsers(data);
    });
};
  const handleActivate = (id) => {
    // Mengirim PUT request ke API untuk mengaktifkan pengguna dengan ID tertentu
    axios
      .put(`${apiUrl}/api/User/activateUser/${id}`)
      .then((response) => {
        console.log('Pengguna diaktifkan:', response.data);
        // Refresh daftar pengguna setelah mengaktifkannya
        fetchUsers();
      })
      .catch((error) => {
        console.error('Error activating user:', error);
      });
  };

  const handleDeactivate = (id) => {
    // Mengirim PUT request ke API untuk menonaktifkan pengguna dengan ID tertentu
    axios
      .put(`${apiUrl}/api/User/notActivateUser/${id}`)
      .then((response) => {
        console.log('Pengguna dinonaktifkan:', response.data);
        // Refresh daftar pengguna setelah menonaktifkannya
        fetchUsers();
  console.log(response);

      })
      .catch((error) => {
        console.error('Error deactivating user:', error);
      });
  };
  return (
    <div>
      <Link to="/adminCreateUser">
        <Button
          variant="contained"
          color="primary"
          style={{ marginBottom: '16px' }}
        >
          Tambah User
        </Button>
      </Link>
      <TableContainer component={Paper}>
        <Table sx={{ pb: 15, minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Username</TableCell>
              <TableCell align="center">Email</TableCell>
              <TableCell align="center">Status</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((row) => (
              <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell align="center" component="th" scope="row">
                  {row.username}
                </TableCell>
                <TableCell align="center">{row.email}</TableCell>
                <TableCell align="center">
                  {row.isActivated === true ? 'Aktif' : 'Tidak Aktif'}
                </TableCell>
                <TableCell align="center">
                  <Link to={`/adminEditUser/${row.id}`}>
                    <Button variant="contained">Edit</Button>
                  </Link>
                </TableCell>
                <TableCell align="center">
                  {row.isActivated === true ? (
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => handleDeactivate(row.id)}
                    >
                      Non-Active
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => handleActivate(row.id)}
                    >
                      Active
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default UserComponent;
