import React, { useContext } from 'react';
import { CartContext } from '../../contexts/CartContext';

const CartPopup = () => {
  const { cartItems } = useContext(CartContext); // Ambil data keranjang dari konteks
  return (
    <div>
      <h2 color="red">Your Cart</h2>
      <ul>
        {cartItems && cartItems.length > 0 && (
          <ul>
            {cartItems.map((item) => (
              <li key={item.id}>
                {item.title} ({item.schedule})
              </li>
            ))}
          </ul>
        )}
      </ul>
    </div>
  );
};

export default CartPopup;
