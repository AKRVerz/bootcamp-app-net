import React, { useEffect, useState } from "react";
import {
  Box,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import Cookies from "js-cookie";
import jwt_decode from "jwt-decode";

const InvoiceDetail = () => {
  const {id} = useParams();
  const [userId, setUserId] = useState(null);
  const [idInvoice, setIdInvoice] = useState(null);
  const [invoiceDetails, setInvoiceDetails] = useState([]);
  const [totalPayment, setTotalPayment] = useState(0);
  const [userDataLoading, setUserDataLoading] = useState(true);
  const [userDataError, setUserDataError] = useState(null);

  const apiUrl = process.env.REACT_APP_API_URL;

  useEffect(() => {
    // Mendapatkan token dari cookies
    const token = Cookies.get("payload");
    console.log("Token:", token);

    // Mendekode token untuk mendapatkan username
    const decodedToken = jwt_decode(token);
    console.log("Decoded Token:", decodedToken);

    if (!token) {
      console.error("Token tidak tersedia.");
      return;
    }

    const username = decodedToken.name;
    console.log(username);

    if (!username) {
      console.error("ID pengguna tidak valid.");
      return;
    }

    // Set userId menggunakan fetch data dari API
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${apiUrl}/api/User/GetByUsername/${username}`,
        );

        console.log("Response from API:", response);

        const userData = response.data;
        console.log("User Data:", userData);

        console.log("User ID:", userData.id);

        setUserId(userData.id);
        setIdInvoice(id);
        setUserDataLoading(false);
      } catch (error) {
        console.error("Error fetching user data:", error);
        setUserDataError(error);
        setUserDataLoading(false);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${apiUrl}/api/Invoice/GetByIdDetail?UserId=${userId}&idInvoice=${idInvoice}`,
        );
        const data = response.data;
        console.log(id);
        setInvoiceDetails(data);

        const total = data.reduce((acc, detail) => acc + detail.price, 0);
        setTotalPayment(total);
      } catch (error) {
        console.error("Error fetching invoice details from API:", error);
      }
    };

    fetchData();
  }, [userId, idInvoice]);

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "50vh",
        fontFamily: "monospace",
      }}
    >
      <TableContainer component={Paper} sx={{ width: "80%", margin: "auto" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            margin: "0px 10px",
          }}
        >
          <Link to="/" style={{ textDecoration: "none" }}>
            <Typography
              variant="h6"
              sx={{
                color: "#828282",
                margin: 0,
                textAlign: "center",
                fontWeight: "bold",
              }}
            >
              Home &gt;
            </Typography>
          </Link>
          <Link to="/invoice" style={{ textDecoration: "none" }}>
            <Typography
              variant="h6"
              sx={{
                color: "#828282",
                margin: 0,
                textAlign: "center",
                fontWeight: "bold",
              }}
            >
              Invoice &gt;
            </Typography>
          </Link>
          <Typography
            variant="h6"
            sx={{
              color: "#790B0A",
              margin: 0,
              fontWeight: "700",
              textAlign: "center",
            }}
          >
            Details Invoice
          </Typography>
        </Box>
        <Typography
          variant="h5"
          sx={{
            color: "#4F4F4F",
            margin: "20px 10px",
            borderRadius: "0px",
            borderBottom: "none",
            fontSize: "24px",
            fontWeight: "bold",
          }}
        >
          Details Invoice
        </Typography>
        <Box
          sx={{
            margin: "20px 10px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <div>
            <Typography variant="h6">Invoice Number: {idInvoice}</Typography>
          </div>
          <div>
            <Typography
              variant="h6"
              sx={{
                color: "#790B0A",
                margin: 0,
                fontWeight: "700",
                textAlign: "center",
              }}
            >
              Total Payment: IDR {totalPayment}
            </Typography>
          </div>
        </Box>

        <Table sx={{ width: "100%" }}>
          <TableHead sx={{ backgroundColor: "#790B0A" }}>
            <TableRow>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center",
                }}
              >
                No
              </TableCell>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center",
                }}
              >
                Course Name
              </TableCell>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center",
                }}
              >
                Type
              </TableCell>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center",
                }}
              >
                Schedule
              </TableCell>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center",
                }}
              >
                Price
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {invoiceDetails.map((detail, index) => (
              <TableRow key={detail.id}>
                <TableCell
                  sx={{
                    fontSize: "16px",
                    fontWeight: 500,
                    textAlign: "center",
                  }}
                >
                  {index + 1}
                </TableCell>
                <TableCell
                  sx={{
                    fontSize: "16px",
                    fontWeight: 500,
                    textAlign: "center",
                  }}
                >
                  {detail.title}
                </TableCell>
                <TableCell
                  sx={{
                    fontSize: "16px",
                    fontWeight: 500,
                    textAlign: "center",
                  }}
                >
                  Online
                </TableCell>
                <TableCell
                  sx={{
                    fontSize: "16px",
                    fontWeight: 500,
                    textAlign: "center",
                  }}
                >
                  {detail.schedule}
                </TableCell>
                <TableCell
                  sx={{
                    fontSize: "16px",
                    fontWeight: 500,
                    textAlign: "center",
                  }}
                >
                  {`IDR ${detail.price}`}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default InvoiceDetail;
