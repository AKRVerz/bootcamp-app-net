import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Box,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
  Paper,
} from "@mui/material";
import axios from "axios";
import Cookies from "js-cookie";
import jwt_decode from "jwt-decode";

const MainInvoice = () => {
  const [user, setUser] = useState([]);
  const [invoices, setInvoices] = useState([]);
  const [userId, setUserId] = useState(null);
  const url = process.env.REACT_APP_API_URL;

  // // get id User
  // axios.get("namaApi").then((rest) => {
  //   setUser(rest.data);
  // });

  // // DeleteCartByUserId
  // axios.delete("namaApi", { user }).then((res) => {
  //   console.log(res);
  //   console.log(res.data);
  // });

  useEffect(() => {
    // Mendapatkan token dari cookie
    const token = Cookies.get("payload");

    // Mendekode token untuk mendapatkan username
    const decodedToken = jwt_decode(token);
    const username = decodedToken.name;

    // Memastikan token dan username valid
    if (!token || !username) {
      console.error("Token atau ID pengguna tidak valid.");
      return;
    }

    // Mendapatkan userId berdasarkan username
    const fetchUserId = async () => {
      try {
        const response = await axios.get(
          `${url}/api/User/GetByUsername/${username}`,
        );

        console.log("Response from API:", response);

        const userData = response.data;
        console.log("User Data:", userData);

        console.log("User ID:", userData.id);
        setUserId(userData.id);
      } catch (error) {
        console.error("Error fetching user data:", error);
      }
    };

    // Memanggil fungsi untuk mendapatkan userId
    fetchUserId();
  }, []); // Dependensi kosong agar hanya dijalankan sekali setelah render pertama

  useEffect(() => {
    // Mendapatkan invoice berdasarkan userId setelah mendapatkan userId
    const fetchInvoices = async () => {
      try {
        const apiUrl = `${url}/api/Invoice/GetByUser?id=${userId}`;
        const response = await axios.get(apiUrl);
        setInvoices(response.data);
      } catch (error) {
        console.error("Error fetching invoices:", error);
      }
    };

    // Memanggil fungsi untuk mendapatkan invoice setelah mendapatkan userId
    if (userId) {
      fetchInvoices();
    }
  }, [userId]);

  const postDetail = async (invoiceId, cartData) => {
    try {
      const apiUrl = `${url}/api/Invoice/postDetail`;
      const token = Cookies.get("payload");
      const decodedToken = jwt_decode(token);
      const username = decodedToken.name;

      const response = await axios.post(apiUrl, {
        invoice_Id: invoiceId,
        id_User: userId,
        cartItems: [
          {
            cartId: cartData.id,
            id_User: userId,
            schedule_Id: cartData.id_Schedule,
            course_id: cartData.course_id,
            scheduleDate: cartData.jadwal,
            title: cartData.title,
            description: cartData.description,
            price: cartData.price,
            merkId: cartData.merkId,
            imagePath: cartData.imagePath,
          },
        ],
      });

      console.log("Response from API:", response);
      // Handle successful response, jika diperlukan
    } catch (error) {
      console.error("Error posting invoice detail:", error);
      // Handle error, jika diperlukan
    }
  };
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "50vh",
        fontFamily: "monospace",
      }}
    >
      <TableContainer component={Paper} sx={{ width: "80%", margin: "auto" }}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Link to="/" style={{ textDecoration: "none" }}>
            <Typography
              variant="h6"
              sx={{
                color: "#828282",
                margin: 0,
                textAlign: "center",
                fontWeight: "bold",
              }}
            >
              Home &gt;
            </Typography>
          </Link>
          <Typography
            variant="h6"
            sx={{
              color: "#790B0A",
              margin: 0,
              fontWeight: "bold",
              textAlign: "center",
            }}
          >
            Invoice
          </Typography>
        </Box>
        <Typography
          variant="h5"
          sx={{
            color: "#4F4F4F",
            margin: "20px 0",
            borderRadius: "0px",
            borderBottom: "none",
            fontSize: "24px",
            fontWeight: "bold",
          }}
        >
          Menu Invoice
        </Typography>
        <Table sx={{ width: "100%" }}>
          <TableHead sx={{ backgroundColor: "#790B0A" }}>
            <TableRow>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center", // Mengatur tulisan menjadi di tengah
                }}
              >
                No
              </TableCell>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center", // Mengatur tulisan menjadi di tengah
                }}
              >
                No. Invoice
              </TableCell>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center", // Mengatur tulisan menjadi di tengah
                }}
              >
                Date
              </TableCell>
              {/* <TableCell
                sx={{
                  fontWeight: 500,
                  color: 'white',
                  fontSize: '16px',
                  textAlign: 'center', // Mengatur tulisan menjadi di tengah
                }}
              >
                Total Course
              </TableCell>
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: 'white',
                  fontSize: '16px',
                  textAlign: 'center', // Mengatur tulisan menjadi di tengah
                }}
              >
                Total Price
              </TableCell> */}
              <TableCell
                sx={{
                  fontWeight: 500,
                  color: "white",
                  fontSize: "16px",
                  textAlign: "center", // Mengatur tulisan menjadi di tengah
                }}
              >
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {invoices.map((invoice, index) => (
              <TableRow
                key={invoice.idInvoice}
                sx={{
                  backgroundColor: index + (1 % 2) === 0 ? "#f2e7e7" : "white",
                }}
              >
                <TableCell
                  sx={{
                    fontSize: "16px",
                    fontWeight: 500,
                    textAlign: "center", // Mengatur tulisan menjadi di tengah
                  }}
                >
                  {index + 1}
                </TableCell>
                <TableCell
                  sx={{
                    fontSize: "16px",
                    fontWeight: 500,
                    textAlign: "center", // Mengatur tulisan menjadi di tengah
                  }}
                >
                  {invoice.noInvoice}
                </TableCell>
                <TableCell
                  sx={{
                    fontSize: "16px",
                    fontWeight: 500,
                    textAlign: "center", // Mengatur tulisan menjadi di tengah
                  }}
                >
                  {invoice.date}
                </TableCell>
                {/* <TableCell
                  sx={{
                    fontSize: '16px',
                    fontWeight: 500,
                    textAlign: 'center', // Mengatur tulisan menjadi di tengah
                  }}
                >
                  {invoice.totalCourse}
                </TableCell>
                <TableCell
                  sx={{
                    fontSize: '16px',
                    fontWeight: 500,
                    textAlign: 'center', // Mengatur tulisan menjadi di tengah
                  }}
                >
                  {`IDR ${invoice.totalPrice}`}
                </TableCell> */}
                <TableCell
                  sx={{
                    textAlign: "center", // Mengatur tulisan menjadi di tengah
                  }}
                >
                  <Link to={`/invoice-detail/${invoice.idInvoice}`}>
                    <Button
                      variant="outlined"
                      color="error"
                      // onClick={() => postDetail(invoice.idInvoice, invoice)}
                    >
                      Detail
                    </Button>
                  </Link>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default MainInvoice;
