import React, { useState } from 'react';
import { TextField, Button, Snackbar, Box, RadioGroup, FormControlLabel, Radio } from '@mui/material';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const CreateCategoryComponent = () => {
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [deskripsi, setdeskripsi] = useState('');
  const [imagePath, setImagePath] = useState('');
  const [isActive, setisActive] = useState('true');

  const [snackbarOpen, setSnackbarOpen] = useState(false);
const handleImageChange = (e) => {
  const file = e.target.files[0];

  if (file) {
    const reader = new FileReader();

    reader.onload = (event) => {
      const dataURL = event.target.result;
      // Mengonversi data URL menjadi byte dengan menghapus "data:image/jpeg;base64,"
      const bytes = dataURL.split(',')[1];

      // Simpan byte dalam state
      setImagePath(bytes);
    };

    // Membaca file sebagai data URL
    reader.readAsDataURL(file);
  }
};
  const handleSubmit = (e) => {
    e.preventDefault();

    const payload = {
      categoryname: name,
      deskripsi: deskripsi,
      ImagePath: imagePath,
      isActive: isActive === 'true',
    };
const apiUrl = process.env.REACT_APP_API_URL;
    axios
      .post(`${apiUrl}/api/Category`, payload)
      .then((response) => {
        console.log('Category created successfully:', response.data);
        setSnackbarOpen(true);
        setTimeout(() => {
          navigate('/adminCategory');
        }, 2000);
      })
      .catch((error) => {
        console.error('Error creating category:', error);
      });
  };

  return (
    <Box p={3}>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <h2>Create Category</h2>
        <TextField
          label="Name"
          required
          variant="outlined"
          color="secondary"
          fullWidth
          value={name}
          onChange={(e) => setName(e.target.value)}
          sx={{ mb: 2 }}
        />
        <TextField
          label="deskripsi"
          required
          variant="outlined"
          color="secondary"
          fullWidth
          value={deskripsi}
          onChange={(e) => setdeskripsi(e.target.value)}
          sx={{ mb: 2 }}
        />
        <Box sx={{ mt: 3 }}>
          <Box>
            <Box
              component="img"
              sx={{ height: '140px' }}
              src={`data:image/png;base64,${imagePath}`}
            />
          </Box>
          <TextField
            type="file"
            inputProps={{ accept: 'image/*' }}
            onChange={handleImageChange}
          />
        </Box>
        <div style={{ marginTop: '16px' }}>
          <RadioGroup
            row
            name="isActive"
            value={isActive}
            onChange={(e) => setisActive(e.target.value === 'true')}
          >
            <FormControlLabel
              value="true"
              control={<Radio color="primary" />}
              label="Aktif"
            />
            <FormControlLabel
              value="false"
              control={<Radio color="primary" />}
              label="Tidak Aktif"
            />
          </RadioGroup>
        </div>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          style={{ marginTop: '16px' }}
        >
          Save
        </Button>
      </form>
      {/* Snackbar for notification */}
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message="Category berhasil dibuat!"
        sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF', mt: 2 }}
      />
    </Box>
  );
};

export default CreateCategoryComponent;
