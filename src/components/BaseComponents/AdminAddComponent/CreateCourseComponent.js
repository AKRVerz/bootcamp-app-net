import React, { useState, useEffect } from 'react';
import {
  TextField,
  Button,
  Snackbar,
  RadioGroup,
  FormControlLabel,
  Radio,
  Box,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const CreateCourseComponent = () => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [categoryId, setCategoryId] = useState('');
  const [imagePath, setImagePath] = useState('');
  const [isActive, setIsActive] = useState(true);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [kategoriOptions, setKategoriOptions] = useState([]); // State untuk menyimpan opsi kategori
  const navigate = useNavigate();
const apiUrl = process.env.REACT_APP_API_URL;
  useEffect(() => {
    axios
      .get(`${apiUrl}/api/Category`)
      .then((response) => {
        // Menyusun data menjadi bentuk objek { value, label } untuk dropdown
        const options = response.data.map((kategori) => ({
          value: kategori.id, // Sesuaikan dengan properti ID kategori di API
          label: kategori.categoryName, // Sesuaikan dengan properti nama kategori di API
        }));
        // Menyimpan opsi kategori ke dalam state
        setKategoriOptions(options);
      })
      .catch((error) => {
        console.error('Error fetching category options:', error);
      });
  }, []);

  const handleImageChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (event) => {
        const dataURL = event.target.result;
        // Mengonversi data URL menjadi byte dengan menghapus "data:image/jpeg;base64,"
        const bytes = dataURL.split(',')[1];

        // Simpan byte dalam state
        setImagePath(bytes);
      };

      // Membaca file sebagai data URL
      reader.readAsDataURL(file);
    }
  };

  function base64ToHex(str) {
    const raw = atob(str);
    let hex = '0x';
    let result = '';
    for (let i = 0; i < raw.length; i++) {
      const hex = raw.charCodeAt(i).toString(16);
      result += hex.length === 2 ? hex : '0' + hex;
    }
    return (hex += result.toLowerCase());
  }



  const handleSubmit = (e) => {
    e.preventDefault();

    const payload = {
      title: title,
      description: description,
      price: price,
      merkId: categoryId,
      imagePath: imagePath,
      isActive: isActive,
    };

    axios
      .post(`${apiUrl}/api/Course`, payload)
      .then((response) => {
        console.log('Course created successfully:', response.data);
        setSnackbarOpen(true);

        setTimeout(() => {
          navigate('/admincourse');
        }, 2000);
      })
      .catch((error) => {
        console.error('Error creating course:', error);
      });
  };


  return (
    <Box p={3}>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <h2>Create Course</h2>
        <TextField
          label="Title"
          required
          variant="outlined"
          color="secondary"
          fullWidth
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          sx={{ mb: 2 }}
        />
        <TextField
          label="Description"
          required
          variant="outlined"
          color="secondary"
          fullWidth
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          sx={{ mb: 2 }}
        />
        <TextField
          label="Price"
          required
          variant="outlined"
          color="secondary"
          fullWidth
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          sx={{ mb: 2 }}
        />
        <FormControl fullWidth sx={{ mb: 2 }}>
          <InputLabel id="kategori-label">Kategori</InputLabel>
          <Select
            labelId="kategori-label"
            id="kategori"
            value={categoryId}
            label="Kategori"
            onChange={(e) => setCategoryId(e.target.value)}
          >
            {kategoriOptions.map((kategori) => (
              <MenuItem key={kategori.value} value={kategori.value}>
                {kategori.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Box sx={{ mt: 3 }}>
          <Box>
            <Box
              component="img"
              sx={{ height: '140px' }}
              src={`data:image/png;base64,${imagePath}`}
            />
          </Box>
          <TextField
            type="file"
            inputProps={{ accept: 'image/*' }}
            onChange={handleImageChange}
          />
        </Box>

        <div style={{ marginTop: '16px' }}>
          <RadioGroup
            row
            name="status"
            value={isActive}
            onChange={(e) => setIsActive(e.target.value === 'true')}
          >
            <FormControlLabel
              value="true"
              control={<Radio color="primary" />}
              label="Aktif"
            />
            <FormControlLabel
              value="false"
              control={<Radio color="primary" />}
              label="Tidak Aktif"
            />
          </RadioGroup>
        </div>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          style={{ marginTop: '16px' }}
        >
          Save
        </Button>
      </form>
      {/* Snackbar for notification */}
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message="Course berhasil dibuat!"
        sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF', mt: 2 }}
      />
    </Box>
  );
};

export default CreateCourseComponent;
