import React, { useState } from 'react';
import {
  TextField,
  Button,
  Snackbar,
  FormControlLabel,
  RadioGroup,
  Radio,
  Box,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const CreatePaymentComponent = () => {
  const [name, setName] = useState('');
  const [logo, setLogo] = useState('');
  const [status, setStatus] = useState(true);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const navigate = useNavigate();
const apiUrl = process.env.REACT_APP_API_URL;
  const handleSubmit = (e) => {
    e.preventDefault();

    const payload = {
      name: name,
      logo: logo,
      isActive: status,
    };

    axios
      .post(`${apiUrl}/api/PaymentMethod`, payload)
      .then((response) => {
        console.log('Payment Method created successfully:', response.data);
        setSnackbarOpen(true);

        setTimeout(() => {
          navigate('/adminpayment');
        }, 2000);
      })
      .catch((error) => {
        console.error('Error creating payment method:', error);
      });
  };
  const handleImageChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (event) => {
        const dataURL = event.target.result;
        // Mengonversi data URL menjadi byte dengan menghapus "data:image/jpeg;base64,"
        const bytes = dataURL.split(',')[1];

        // Simpan byte dalam state
        setLogo(bytes);
      };

      // Membaca file sebagai data URL
      reader.readAsDataURL(file);
    }
  };

  return (
    <Box p={3}>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <h2>Create Payment Method</h2>
        <TextField
          label="Name"
          required
          variant="outlined"
          color="secondary"
          fullWidth
          value={name}
          onChange={(e) => setName(e.target.value)}
          sx={{ mb: 2 }}
        />
        <Box sx={{ mt: 3 }}>
          <Box>
            <Box
              component="img"
              sx={{ height: '140px' }}
              src={`data:image/png;base64,${logo}`}
            />
          </Box>
          <TextField
            type="file"
            inputProps={{ accept: 'image/*' }}
            onChange={handleImageChange}
          />
        </Box>
        <div style={{ marginTop: '16px' }}>
          <RadioGroup
            row
            name="status"
            value={status.toString()}
            onChange={(e) => setStatus(e.target.value === 'true')}
          >
            <FormControlLabel
              value="true"
              control={<Radio color="primary" />}
              label="Aktif"
            />
            <FormControlLabel
              value="false"
              control={<Radio color="primary" />}
              label="Tidak Aktif"
            />
          </RadioGroup>
        </div>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          style={{ marginTop: '16px' }}
        >
          Save
        </Button>
      </form>
      {/* Snackbar for notification */}
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message="Payment Method berhasil dibuat!"
        sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF', mt: 2 }}
      />
    </Box>
  );
};

export default CreatePaymentComponent;
