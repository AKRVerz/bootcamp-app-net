import React, { useState } from "react";
import { TextField, FormControl, Button, InputLabel, Select, MenuItem } from "@mui/material";
import axios from 'axios'
import { useNavigate } from "react-router-dom";

const CreateUserComponent = () => {
const [username, setUsername] = useState('');
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [role, setRole] = useState('');
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const navigate = useNavigate();
const apiUrl = process.env.REACT_APP_API_URL;


const handleSubmit = (e) => {
  e.preventDefault();

  const payload = {
    username: username,
    password: password,
    email: email,
    role: role,
  };

  axios
    .post(`${apiUrl}/api/User/CreateUser`, payload)
    .then((response) => {
      console.log('User Created Successfully :', response.data);
      setSnackbarOpen(true);
      setTimeout(() => {
        navigate('/adminUser');
      }, 2000);
    })
    .catch((error) => {
      console.error('Error creating payment method:', error);
    });
}

  return (
    <div>
      <React.Fragment>
        <form autoComplete="off" onSubmit={handleSubmit}>
          <h2>Create User</h2>
          <TextField
            label="Username"
            // onChange={e => setEmail(e.target.value)}
            required
            variant="outlined"
            color="secondary"
            type="string"
            sx={{ mb: 3 }}
            fullWidth
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            // value={email}
            // error={emailError}
          />
          <TextField
            label="Password"
            //onChange={e => setPassword(e.target.value)}
            required
            variant="outlined"
            color="secondary"
            type="string"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            // value={password}
            // error={passwordError}
            fullWidth
            sx={{ mb: 3 }}
          />
          <TextField
            label="Email"
            //onChange={e => setPassword(e.target.value)}
            required
            variant="outlined"
            color="secondary"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            // value={password}
            // error={passwordError}
            fullWidth
            sx={{ mb: 3 }}
          />
          <FormControl fullWidth sx={{ mb: 3 }}>
            <InputLabel>Role</InputLabel>
            <Select
              label="Role"
              value={role}
              onChange={(e) => setRole(e.target.value)}
              fullWidth
            >
              <MenuItem value="user">User</MenuItem>
              <MenuItem value="admin">Admin</MenuItem>
            </Select>
          </FormControl>
          <Button variant="contained" color="success" type="submit">
            Save
          </Button>
        </form>
      </React.Fragment>
    </div>
  );
};

export default CreateUserComponent;
