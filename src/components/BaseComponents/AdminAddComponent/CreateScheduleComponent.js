import React, { useState, useEffect } from 'react';
import {
  TextField,
  Button,
  Snackbar,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Box,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const CreateScheduleComponent = () => {
  const [jadwal, setJadwal] = useState('');
  const [courseFK, setCourseFK] = useState('');
  const [courses, setCourses] = useState([]);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const navigate = useNavigate();
  const apiUrl = process.env.REACT_APP_API_URL;

  useEffect(() => {
    // Mengambil data course dari API saat komponen dimuat
    axios
      .get(`${apiUrl}/api/Course`)
      .then((response) => {
        setCourses(response.data);
      })
      .catch((error) => {
        console.error('Error fetching course data:', error);
      });
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

    const payload = {
      jadwal: jadwal,
      courseFK: courseFK,
    };

    axios
      .post(`${apiUrl}/api/Schedule`, payload)
      .then((response) => {
        console.log('Schedule created successfully:', response.data);
        setSnackbarOpen(true);

        setTimeout(() => {
          navigate('/adminschedule');
        }, 2000);
      })
      .catch((error) => {
        console.error('Error creating schedule:', error);
      });
  };

  return (
    <Box p={3}>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <h2>Create Schedule</h2>
        <TextField
          label="Tanggal"
          required
          variant="outlined"
          color="secondary"
          type="date"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={jadwal}
          onChange={(e) => setJadwal(e.target.value)}
          sx={{ mb: 2 }}
        />
        <FormControl fullWidth variant="outlined" sx={{ mb: 2 }}>
          <InputLabel id="courseFK-label">Course</InputLabel>
          <Select
            labelId="courseFK-label"
            label="Course"
            value={courseFK}
            onChange={(e) => setCourseFK(e.target.value)}
          >
            {courses.map((course) => (
              <MenuItem key={course.id} value={course.id}>
                {course.title}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          style={{ marginTop: '16px' }}
        >
          Save
        </Button>
      </form>
      {/* Snackbar for notification */}
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message="Schedule berhasil dibuat!"
        sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF', mt: 2 }}
      />
    </Box>
  );
};

export default CreateScheduleComponent;
