import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import useScheduleData from '../../hooks/useScheduleById';

const ScheduleDropdown = ({ onSelectSchedule, courseId }) => {
  // Use custom hook to get schedule data
  const { schedules, loading, error } = useScheduleData(courseId);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (schedules && schedules.length > 0) {
    return (
      <div>
        <FormControl variant="outlined" fullWidth>
          <InputLabel>Select a Schedule</InputLabel>
          <Select
            label="Select a Schedule"
            onChange={(e) => onSelectSchedule(e.target.value)}
          >
            <MenuItem value="">
              <em>Select Schedule</em>
            </MenuItem>
            {schedules.map((schedule) => (
              <MenuItem key={schedule.id} value={schedule.id}>
                {schedule.scheduleProperty}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    );
  } else {
    return <div>No schedules available.</div>;
  }
};

ScheduleDropdown.propTypes = {
  onSelectSchedule: PropTypes.func.isRequired,
  courseId: PropTypes.string.isRequired,
};

export default ScheduleDropdown;
