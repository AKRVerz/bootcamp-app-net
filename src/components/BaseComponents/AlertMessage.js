import React from 'react';
import Alert from '@mui/material/Alert';

const AlertMessage = ({ alert, onClose }) => {
  return (
    alert.open && (
      <Alert
        severity={alert.type}
        onClose={onClose}
        sx={{
          width: '100%',
          marginBottom: '16px',
        }}
        variant="outlined"
      >
        {alert.message}
      </Alert>
    )
  );
};

export default AlertMessage;
