import React from 'react';
import TextField from '@mui/material/TextField';

const TextInput = (props) => {
  const { label, type, name, value, onChange, onBlur, error, helperText } =
    props;

  return (
    <TextField
      label={label}
      type={type}
      variant="outlined"
      fullWidth
      name={name}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      error={error}
      helperText={helperText}
      sx={{ margin: '8px 0 10px 0' }}
    />
  );
};

export default TextInput;
