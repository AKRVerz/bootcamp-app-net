import React, { useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Collapse from '@mui/material/Collapse';
import { useMediaQuery } from '@mui/material';
import { useNavigate, Link } from 'react-router-dom';
import ToggleTheme from '../../Temp/ToggleTheme';

const Header = ({ showButtons }) => {
  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const [isMenuOpen, setMenuOpen] = useState(false);
  const navigate = useNavigate();

  const toggleMenu = () => {
    setMenuOpen(!isMenuOpen);
  };

  // Fungsi untuk mengarahkan ke halaman login
  const goToLoginPage = () => {
    // Menggunakan navigate untuk navigasi
    navigate('/login');
  };

  // Fungsi untuk mengarahkan ke halaman register
  const goToRegisterPage = () => {
    // Menggunakan navigate untuk navigasi
    navigate('/register');
  };

  return (
    <AppBar
      position="static"
      elevation={0}
      sx={{ backgroundColor: 'white', padding: '8px 50px' }}
    >
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Link to="/" style={{ textDecoration: 'none', color: 'black' }}>
          <Typography
            variant="h6"
            sx={{
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <img
              src="/logo.png"
              alt="Logo"
              style={{ marginRight: '8px', width: '40px', height: '40px' }}
            />
            Otomobil
          </Typography>
        </Link>
        <ToggleTheme />

        <div style={{ position: 'relative' }}>
          {isMobile ? (
            <>
              {showButtons && (
                <Button
                  style={{
                    color: '#790B0A',
                    marginBottom: '10px',
                  }}
                  onClick={toggleMenu}
                >
                  <i className="material-icons">menu</i>
                </Button>
              )}
              <Collapse
                in={isMenuOpen}
                style={{
                  position: 'absolute',
                  top: '100%',
                  left: 0,
                  backgroundColor: 'white',
                  zIndex: 1,
                  boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
                }}
              >
                {showButtons && (
                  <>
                    <Button
                      style={{
                        fontSize: '10px',
                        color: '#790B0A',
                        marginBottom: '10px',
                      }}
                      onClick={goToLoginPage} // Mengarahkan ke halaman login saat tombol "Login" ditekan
                    >
                      Login
                    </Button>
                    <Button
                      sx={{
                        fontSize: '10px',
                        backgroundColor: '#790B0A',
                        color: 'white',
                        marginBottom: '10px',
                        '&:hover': {
                          backgroundColor: '#790B0A',
                        },
                      }}
                      onClick={goToRegisterPage} // Mengarahkan ke halaman register saat tombol "Sign Up" ditekan
                    >
                      Sign Up
                    </Button>
                  </>
                )}
              </Collapse>
            </>
          ) : (
            <>
              {showButtons && (
                <>
                  <Button
                    style={{
                      color: '#790B0A',
                      marginRight: '10px',
                    }}
                    onClick={goToLoginPage} // Mengarahkan ke halaman login saat tombol "Login" ditekan
                  >
                    Login
                  </Button>
                  <Button
                    sx={{
                      backgroundColor: '#790B0A',
                      color: 'white',
                      '&:hover': {
                        backgroundColor: '#790B0A',
                      },
                    }}
                    onClick={goToRegisterPage} // Mengarahkan ke halaman register saat tombol "Sign Up" ditekan
                  >
                    Sign Up
                  </Button>
                </>
              )}
            </>
          )}
        </div>
      </div>
    </AppBar>
  );
};

export default Header;
