import React, { useState } from 'react';
import Box from '@mui/system/Box';
import Typography from '@mui/material/Typography';
import { Alert, useMediaQuery } from '@mui/material';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import RegisterForm from './RegistrationForm';
import SuccessModal from '../SuccessModal';
import ErrorModal from '../ErrorModal';

const MainRegisterPage = () => {
  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));

  const apiUrl = process.env.REACT_APP_API_URL;
  const navigate = useNavigate();
  const [isSuccessModalOpen, setIsSuccessModalOpen] = useState(false);
  const [isErrorModalOpen, setIsErrorModalOpen] = useState(false);
  const [alert, setAlert] = useState({
    open: false,
    type: 'success',
    message: '',
  });

  const goToLoginPage = () => {
    navigate('/login');
  };

  const registerUser = (userData) => {
    axios
      .post(`${apiUrl}/api/User/CreateUser`, userData)
      .then((response) => {
        if (response.status === 201) {
          setIsSuccessModalOpen(true);
        } else {
          setAlert({
            open: true,
            type: 'error',
            message: 'Failed to create account.',
          });
          setIsErrorModalOpen(true);
          console.error('Pendaftaran gagal');
        }
      })
      .catch((error) => {
        setAlert({ open: true, type: 'error', message: 'An error occurred.' });
        setIsErrorModalOpen(true);
        console.error('Terjadi kesalahan:', error);
      });
  };

  const handleCloseAlert = () => {
    setAlert({ ...alert, open: false });
  };

  const handleCloseSuccessModal = () => {
    setIsSuccessModalOpen(false);
  };

  const handleCloseErrorModal = () => {
    setIsErrorModalOpen(false);
  };

  return (
    <Box
      sx={{
        width: isMobile ? '90%' : '616px',
        height: 'auto',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        padding: isMobile ? '20px 10px' : '20px',
        border: 'none',
        boxShadow: 'none',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        position: 'absolute',
      }}
    >
      <Typography
        variant="h2"
        sx={{
          color: '#790B0A',
          fontSize: isMobile ? '20px' : '24px',
          margin: '0 0 16px 0',
        }}
      >
        Let's Join our course!
      </Typography>
      <Typography
        variant="body1"
        sx={{
          color: '#4F4F4F',
          margin: '0 0 16px 0',
          opacity: '0.7',
        }}
      >
        Please register first
      </Typography>
      <RegisterForm onSubmit={registerUser} />
      {alert.open && (
        <Alert
          severity={alert.type}
          onClose={handleCloseAlert}
          sx={{
            width: '100%',
            marginBottom: '16px',
          }}
          variant="outlined"
        >
          {alert.message}
        </Alert>
      )}

      <SuccessModal
        open={isSuccessModalOpen}
        onClose={handleCloseSuccessModal}
        onGoToLogin={goToLoginPage}
      />

      <ErrorModal open={isErrorModalOpen} onClose={handleCloseErrorModal} />

      <Typography
        variant="body1"
        sx={{
          alignSelf: 'center',
          fontSize: isMobile ? '12px' : '16px',
          fontWeight: 400,
          marginTop: '60px',
          '& .blue-text': {
            color: 'blue',
            cursor: 'pointer',
            transition: 'color 0.3s',
            '&:hover': {
              color: 'darkblue',
            },
          },
        }}
      >
        Have an account?
        <span className="blue-text" onClick={goToLoginPage}>
          {' '}
          Login here{' '}
        </span>
      </Typography>
    </Box>
  );
};

export default MainRegisterPage;
