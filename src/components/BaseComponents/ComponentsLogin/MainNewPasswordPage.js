import React, { useState } from 'react';
import Box from '@mui/system/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useMediaQuery } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import useNewPassword from '../../../hooks/useNewPassword';
import Popup from '../Popup';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

const MainNewPasswordPage = () => {
  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const navigate = useNavigate();
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [showPassword, setShowPassword] = useState(false); // State to toggle password visibility

  // Inisialisasi custom hook
  const { isLoading, success, error, resetPassword } = useNewPassword();

  const [popupOpen, setPopupOpen] = useState(false);
  const [popupMessage, setPopupMessage] = useState('');

  const handlePopupClose = () => {
    setPopupOpen(false);
  };

  const togglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  const goToResetPasswordPage = () => {
    navigate('/reset-password');
  };

  const goToLoginPage = () => {
    if (newPassword.trim() === '') {
      setPasswordError('New Password is required');
    } else if (confirmNewPassword.trim() === '') {
      setPasswordError('Confirm New Password is required');
    } else if (newPassword !== confirmNewPassword) {
      setPasswordError('Passwords do not match');
    } else {
      // Panggil resetPassword dengan password dan confirmPassword yang sesuai
      resetPassword(newPassword, confirmNewPassword)
        .then(() => {
          setPopupMessage('Password berhasil diperbarui.');
          setPopupOpen(true);
        })
        .catch((error) => {
          setPopupMessage('Password gagal diperbarui.');
          setPopupOpen(true);
        });
    }
  };

  return (
    <Box
      sx={{
        width: isMobile ? '90%' : '616px',
        height: 'auto',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        padding: isMobile ? '20px 10px' : '20px',
        border: 'none',
        boxShadow: 'none',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        position: 'absolute',
      }}
    >
      <Typography
        variant="h2"
        sx={{
          color: '#790B0A',
          fontSize: isMobile ? '20px' : '24px',
          margin: '0 0 16px 0',
        }}
      >
        Create Password
      </Typography>

      <TextField
        label="New Password"
        variant="outlined"
        fullWidth
        type={showPassword ? 'text' : 'password'} // Toggle password visibility
        sx={{
          width: isMobile ? '100%' : '616px',
          height: '34px',
          margin: '15px 0',
          borderRadius: '4px',
        }}
        value={newPassword}
        onChange={(e) => setNewPassword(e.target.value)}
        error={!!passwordError}
        helperText={passwordError}
        InputProps={{
          // Add eye icon for toggling visibility
          endAdornment: (
            <div
              style={{ cursor: 'pointer' }}
              onClick={togglePasswordVisibility}
            >
              {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
            </div>
          ),
        }}
      />
      <TextField
        label="Confirm New Password"
        variant="outlined"
        fullWidth
        type={showPassword ? 'text' : 'password'} // Toggle password visibility
        sx={{
          width: isMobile ? '100%' : '616px',
          height: '34px',
          margin: '15px 0',
          borderRadius: '4px',
        }}
        value={confirmNewPassword}
        onChange={(e) => setConfirmNewPassword(e.target.value)}
        error={!!passwordError}
        helperText={passwordError}
        InputProps={{
          // Add eye icon for toggling visibility
          endAdornment: (
            <div
              style={{ cursor: 'pointer' }}
              onClick={togglePasswordVisibility}
            >
              {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
            </div>
          ),
        }}
      />
      <div
        style={{
          alignSelf: 'flex-end',
          marginTop: '20px',
        }}
      >
        <Button
          onClick={goToResetPasswordPage}
          variant="contained"
          color="error"
          sx={{
            width: '100px',
            height: '38px',
            borderRadius: '8px',
            padding: '10px',
            backgroundColor: 'white',
            color: '#790B0A',
            borderColor: '#790B0A',
            '&:hover': {
              backgroundColor: '#FADDDD',
            },
            marginRight: '16px',
          }}
        >
          Cancel
        </Button>
        <Button
          onClick={goToLoginPage}
          variant="contained"
          color="primary"
          disabled={isLoading}
          sx={{
            width: '100px',
            height: '38px',
            borderRadius: '8px',
            padding: '10px',
            backgroundColor: '#790B0A',
            color: 'white',
            '&:hover': {
              backgroundColor: '#8E0C10',
            },
          }}
        >
          {isLoading ? 'Sending...' : 'Confirm'}
        </Button>
      </div>

      {/* Tampilkan Popup */}
      <Popup
        open={popupOpen}
        message={popupMessage}
        onClose={handlePopupClose}
      />
    </Box>
  );
};

export default MainNewPasswordPage;
