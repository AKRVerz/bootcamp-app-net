import React, { useState } from 'react';
import Box from '@mui/system/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import { useMediaQuery } from '@mui/material';
import useResetPassword from '../../../hooks/useResetPassword';
import Popup from '../Popup';

const MainResetPasswordPage = () => {
  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');

  // Initialize the custom hook
  const { isLoading, success, error, resetPassword } = useResetPassword();

  const [popupOpen, setPopupOpen] = useState(false);
  const [popupMessage, setPopupMessage] = useState('');

  const handlePopupClose = () => {
    setPopupOpen(false);
  };

  const goToNewPasswordPage = () => {
    if (!email.trim()) {
      setEmailError('Email is required');
    } else {
      setEmailError('');
      resetPassword(email)
        .then(() => {
          setPopupMessage('Email reset password berhasil dikirim.');
          setPopupOpen(true);
        })
        .catch((error) => {
          // Handle the error here
        });
    }
  };

  const goToLoginPage = () => {
    navigate('/login');
  };

  return (
    <Box
      sx={{
        width: isMobile ? '90%' : '616px',
        height: 'auto',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        padding: isMobile ? '20px 10px' : '20px',
        border: 'none',
        boxShadow: 'none',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        position: 'absolute',
      }}
    >
      <Typography
        variant="h2"
        sx={{
          color: '#790B0A',
          fontSize: isMobile ? '20px' : '24px',
          margin: '0 0 16px 0',
        }}
      >
        Reset Password
      </Typography>
      <Typography
        variant="body1"
        sx={{
          color: '#4F4F4F',
          margin: '0 0 16px 0',
          opacity: '0.7',
        }}
      >
        Send OTP code to your email address
      </Typography>
      <TextField
        label="Email"
        variant="outlined"
        fullWidth
        sx={{
          width: isMobile ? '100%' : '616px',
          height: '34px',
          margin: '15px 0',
          borderRadius: '4px',
        }}
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        error={!!emailError}
        helperText={emailError}
      />
      <div
        style={{
          alignSelf: 'flex-end',
          marginTop: '20px',
        }}
      >
        <Button
          variant="contained"
          onClick={goToLoginPage}
          color="error"
          sx={{
            width: '100px',
            height: '38px',
            borderRadius: '8px',
            padding: '10px',
            backgroundColor: 'white',
            color: '#790B0A',
            borderColor: '#790B0A',
            '&:hover': {
              backgroundColor: '#FADDDD',
            },
            marginRight: '16px',
          }}
        >
          Cancel
        </Button>
        <Button
          variant="contained"
          onClick={goToNewPasswordPage}
          color="primary"
          disabled={isLoading}
          sx={{
            width: '100px',
            height: '38px',
            borderRadius: '8px',
            padding: '10px',
            backgroundColor: '#790B0A',
            color: 'white',
            '&:hover': {
              backgroundColor: '#8E0C10',
            },
          }}
        >
          {isLoading ? 'Sending...' : 'Confirm'}
        </Button>
        {/* Tampilkan pesan pop-up menggunakan komponen Popup */}
        <Popup
          open={popupOpen}
          message={popupMessage}
          onClose={handlePopupClose}
        />
      </div>
    </Box>
  );
};

export default MainResetPasswordPage;
