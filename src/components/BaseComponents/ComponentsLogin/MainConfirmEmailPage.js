import React, { useEffect } from 'react';
import Box from '@mui/system/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useMediaQuery } from '@mui/material';
import { useNavigate, useLocation } from 'react-router-dom'; // Menggunakan useLocation untuk mendapatkan URL saat ini
import axios from 'axios';

const MainConfirmEmailPage = () => {
  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const navigate = useNavigate();
  const location = useLocation(); // Gunakan useLocation untuk mendapatkan URL saat ini

  const goToLoginPage = () => {
    navigate('/login');
  };

  // Dapatkan nilai userId dan username dari URL saat ini
  const params = new URLSearchParams(location.search);
  const UserID = params.get('userId');
  const Username = params.get('username');

  useEffect(() => {
    const apiUrl = process.env.REACT_APP_API_URL;
    axios
      .get(
        `${apiUrl}/api/User/ActivateUser?userId=${UserID}&username=${Username}`
      )
      .then((response) => {
        console.log('Response:', response.data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }, [UserID, Username]); // Tambahkan UserID dan Username sebagai dependensi useEffect

  return (
    <Box
      sx={{
        width: isMobile ? '90%' : '616px',
        height: 'auto',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        padding: isMobile ? '20px 10px' : '20px',
        border: 'none',
        boxShadow: 'none',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
      }}
    >
      <img
        src="email.svg"
        alt="Confirm Email"
        style={{ width: '435px', height: '437px', marginBottom: '20px' }}
      />

      <Typography
        variant="h2"
        sx={{
          color: '#790B0A',
          fontSize: isMobile ? '20px' : '24px',
          margin: '0 0 16px 0',
        }}
      >
        Email Confirmation Success
      </Typography>
      <Typography
        variant="body1"
        sx={{
          color: '#4F4F4F',
          margin: '0 0 16px 0',
          opacity: '0.7',
        }}
      >
        Your email has been confirmed! Please login first to access the web
      </Typography>

      <Button
        variant="contained"
        color="primary"
        onClick={goToLoginPage}
        sx={{
          width: '140px',
          height: '38px',
          borderRadius: '8px',
          padding: '10px',
          backgroundColor: '#790B0A',
          color: 'white',
          '&:hover': {
            backgroundColor: '#8E0C10',
          },
        }}
      >
        Login Here
      </Button>
    </Box>
  );
};

export default MainConfirmEmailPage;
