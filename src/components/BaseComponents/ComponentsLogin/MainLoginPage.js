import React, { useContext, useState } from 'react';
import Box from '@mui/system/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useMediaQuery } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import axios from 'axios';
import { AuthContext } from '../../../contexts/AuthContext';
import ErrorModal from '../../Temp/ErrorModal';
import SuccessModal from '../../Temp/SuccessModal';
import { loginValidationSchema } from '../../../utils/validationsSchema';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

const MainLoginPage = () => {
  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const navigate = useNavigate();
  const authContext = useContext(AuthContext);
  const [errorModalOpen, setErrorModalOpen] = useState(false);
  const [successModalOpen, setSuccessModalOpen] = useState(false);
  const [modalMessage, setModalMessage] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const apiUrl = process.env.REACT_APP_API_URL;

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema: loginValidationSchema,
    onSubmit: (values) => {
      axios
        .post(apiUrl + '/api/User/Login', values)
        .then((response) => {
          if (response.status === 200) {
            authContext.login(response.data);

            // Memeriksa apakah pengguna memiliki role "admin"
            if (response.data.role === 'admin') {
              navigate('/adminCourse'); // Arahkan ke dashboard admin jika role adalah "admin"
            } else {
              navigate('/'); // Arahkan ke dashboard pengguna jika role bukan "admin"
            }

            setSuccessModalOpen(true);
          } else {
            setModalMessage('Login failed');
            setErrorModalOpen(true);
          }
        })
        .catch((error) => {
          setModalMessage('Data Yang Dimasukan Tidak Sesuai');
          setErrorModalOpen(true);
        });
    },
  });

  const goToRegisterPage = () => {
    navigate('/register');
  };

  const goToForgotPasswordPage = () => {
    navigate('/forgot-password');
  };

  const togglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  return (
    <Box
      sx={{
        width: isMobile ? '90%' : '616px',
        height: '241px',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        padding: isMobile ? '20px 10px' : '20px',
        border: 'none',
        boxShadow: 'none',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        position: 'absolute',
      }}
    >
      <Typography
        variant="h2"
        sx={{
          color: '#790B0A',
          fontSize: isMobile ? '20px' : '24px',
          margin: '0 0 16px 0',
        }}
      >
        Welcome back!
      </Typography>
      <Typography
        variant="body1"
        sx={{
          color: '#4F4F4F',
          margin: '0 0 16px 0',
          opacity: '0.7',
        }}
      >
        Please login first
      </Typography>
      <form onSubmit={formik.handleSubmit}>
        <TextField
          label="username"
          variant="outlined"
          fullWidth
          name="username"
          value={formik.values.username}
          onChange={formik.handleChange}
          sx={{
            width: isMobile ? '100%' : '616px',
            height: '34px',
            margin: '8px 0 20px 0',
            borderRadius: '4px',
          }}
        />
        {formik.touched.username && formik.errors.username ? (
          <Typography variant="body1" sx={{ color: 'red', marginTop: '8px' }}>
            {formik.errors.username}
          </Typography>
        ) : null}
        <TextField
          label="Password"
          type={showPassword ? 'text' : 'password'} // Toggle password visibility
          variant="outlined"
          fullWidth
          name="password"
          value={formik.values.password}
          onChange={formik.handleChange}
          sx={{
            width: isMobile ? '100%' : '616px',
            height: '34px',
            margin: '30px 0 30px 0',
            borderRadius: '4px',
          }}
          InputProps={{
            // Add eye icon for toggling visibility
            endAdornment: (
              <div
                style={{ cursor: 'pointer' }}
                onClick={togglePasswordVisibility}
              >
                {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
              </div>
            ),
          }}
        />
        {formik.touched.password && formik.errors.password ? (
          <Typography variant="body1" sx={{ color: 'red', marginTop: '8px' }}>
            {formik.errors.password}
          </Typography>
        ) : null}
        <Typography
          variant="body1"
          sx={{
            fontSize: isMobile ? '12px' : '16px',
            fontWeight: 400,
            marginTop: '16px',
            '& .blue-text': {
              color: 'blue',
              cursor: 'pointer',
              transition: 'color 0.3s',
              '&:hover': {
                color: 'darkblue',
              },
            },
          }}
        >
          Forgot Password?{' '}
          <span className="blue-text" onClick={goToForgotPasswordPage}>
            Click Here
          </span>
        </Typography>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          sx={{
            float: 'right',
            width: '140px',
            height: '38px',
            borderRadius: '8px',
            padding: '10px',
            backgroundColor: '#790B0A',
            color: 'white',
            '&:hover': {
              backgroundColor: '#790B0A',
            },
            marginTop: '16px',
          }}
        >
          Login
        </Button>
      </form>
      <Typography
        variant="body1"
        sx={{
          alignSelf: 'center',
          fontSize: isMobile ? '12px' : '16px',
          fontWeight: 400,
          marginTop: '60px',
          '& .blue-text': {
            color: 'blue',
            cursor: 'pointer',
            transition: 'color 0.3s',
            '&:hover': {
              color: 'darkblue',
            },
          },
        }}
      >
        Don't have an account?
        <span className="blue-text" onClick={goToRegisterPage}>
          {' '}
          Sign Up here{' '}
        </span>
      </Typography>
      <ErrorModal
        open={errorModalOpen}
        onClose={() => setErrorModalOpen(false)}
        message={modalMessage}
      />
      <SuccessModal
        open={successModalOpen}
        onClose={() => setSuccessModalOpen(false)}
        message="Login successful"
      />
    </Box>
  );
};

export default MainLoginPage;
