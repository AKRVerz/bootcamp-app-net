import React, { useState } from 'react';
import { useFormik } from 'formik';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Button,
  IconButton, // Import IconButton
  InputAdornment, // Import InputAdornment
} from '@mui/material';
import TextInput from './TextInput'; // Buat komponen TextInput seperti yang telah dibahas sebelumnya
import { validationSchema } from '../../../utils/validationsSchema';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

const RegisterForm = ({ onSubmit }) => {
  const formik = useFormik({
    initialValues: {
      username: '',
      email: '',
      password: '',
      role: 'User',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      onSubmit(values); // Panggil fungsi onSubmit yang diterima dari komponen induk
    },
  });
  const [showPassword, setShowPassword] = useState(false);
  const togglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };
  return (
    <form onSubmit={formik.handleSubmit}>
      <TextInput
        label="Username"
        type="text"
        name="username"
        value={formik.values.username}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        error={formik.touched.username && Boolean(formik.errors.username)}
        helperText={formik.touched.username && formik.errors.username}
      />
      <TextInput
        label="Email"
        type="text"
        name="email"
        value={formik.values.email}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        error={formik.touched.email && Boolean(formik.errors.email)}
        helperText={formik.touched.email && formik.errors.email}
      />
      <TextInput
        label="Password"
        type={showPassword ? 'text' : 'password'}
        name="password"
        value={formik.values.password}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        error={formik.touched.password && Boolean(formik.errors.password)}
        helperText={formik.touched.password && formik.errors.password}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={togglePasswordVisibility}
                edge="end"
              >
                {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      {/* <FormControl fullWidth variant="outlined" >
        <InputLabel htmlFor="role-select">Role</InputLabel>
        <Select
          label="Role"
          id="role-select"
          name="role"
          value={formik.values.role}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.role && Boolean(formik.errors.role)}
        >
          <MenuItem value="admin">Admin</MenuItem>
          <MenuItem value="user">User</MenuItem>
        </Select>
      </FormControl> */}
      <Button
        type="submit"
        variant="contained"
        color="primary"
        sx={{
          width: '140px',
          height: '38px',
          borderRadius: '8px',
          padding: '10px',
          backgroundColor: '#790B0A',
          color: 'white',
          '&:hover': {
            backgroundColor: '#790B0A',
          },
          alignSelf: 'flex-end',
          marginTop: '16px',
        }}
      >
        Register
      </Button>
    </form>
  );
};

export default RegisterForm;
