import React, { useState, useEffect } from "react";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import axios from "axios";
import Cookies from "js-cookie";
import jwt_decode from "jwt-decode";

function PaymentModal({ isOpen, onClose }) {
  const [paymentMethods, setPaymentMethods] = useState([]);
  const [selectedMethod, setSelectedMethod] = useState(null);
  const [userId, setUserId] = useState(null);
  const [userDataError, setUserDataError] = useState(null);
  const [scheduleData, setScheduleData] = useState([]);
  const [cartData, setCartData] = useState([]);
  const url = process.env.REACT_APP_API_URL;
  // const [cart, setCart] = useState([]);

  // axios.get("https://localhost:7040/api/Cart").then((rest) => {
  //   setCart(rest.data);
  //   console.log(cart);
  // }, []);

  // useEffect(() => {
  //   fetch("https://localhost:7040/api/Cart")
  //     .then((response) => response.json())
  //     // 4. Setting *dogImage* to the image url that we received from the response above
  //     .then((data) => setCart(data.data));
  //   console.log(cart);
  // }, []);

  useEffect(() => {
    fetchPaymentMethods();
    fetchScheduleData();
  }, []);

  const fetchScheduleData = async () => {
    try {
      const apiUrl = `${url}/api/Schedule`; // URL API Schedule
      const response = await axios.get(apiUrl);
      setScheduleData(response.data); // Set data schedule ke state
    } catch (error) {
      console.error('Error fetching schedule data:', error);
    }
  };

  const fetchPaymentMethods = async () => {
    try {
      const apiUrl = `${url}/api/PaymentMethod/GetActivePaymentMethods`;
      const response = await axios.get(apiUrl);
      setPaymentMethods(response.data);
    } catch (error) {
      console.error("Error fetching payment methods:", error);
    }
  };

  const handleSelectMethod = (methodName) => {
    setSelectedMethod(methodName);
  };

  const handlePaymentComplete = async () => {
    const token = Cookies.get("payload");
    const decodedToken = jwt_decode(token);
    const username = decodedToken.name;

    if (!token || !username) {
      console.error("Token atau ID pengguna tidak valid.");
      return;
    }

    try {
      const response = await axios.get(
        `${url}/api/User/GetByUsername/${username}`,
      );

      console.log("Response from API:", response);

      const userData = response.data;
      console.log("User Data:", userData);

      console.log('User ID:', userData.id);

      if (selectedMethod) {
        const invoiceNumber = generateRandomInvoiceNumber();

        const data = {
          noInvoice: invoiceNumber,
          idUserFK: userData.id,
          payment_method: selectedMethod,
          isPaid: true,
        };
        // const detailOrder={
        //   noInvoice: invoiceNumber,
        //   idUserFK: userData.id,
        //   schedule_Id :
        // };

        console.log("Data untuk dikirim:", data);

        try {
          const apiUrl = `${url}/api/Invoice`;
          await axios.post(apiUrl, data);
          console.log(`Pembayaran selesai dengan metode: ${selectedMethod}`);
          // axios.post("https://localhost:7040/api/Invoice/postDetail").then()
          onClose();
        } catch (error) {
          console.error("Error posting invoice data:", error);
        }

        try{
          const apiUrl = `${url}/api/Cart/${userData.id}`;
          const response = await axios.get(apiUrl);
          const cartData = response.data;

          if (selectedMethod) {
            const DetailData = {
              noInv: invoiceNumber,
              id_User: userData.id,
              Course_ids: cartData.map((item) => item.id_Course),
              Schedule_Ids: cartData.map((item) => item.id_Schedule),
            };
            try{
              const apiUrl = `${url}/api/Invoice/postDetail`;
              axios.post(apiUrl, DetailData);
            }catch(error){
              console.error('Error posting invoice data:', error);
            }

            console.log('Data untuk dikirim:', DetailData);
          } else {
            alert('Silakan pilih metode pembayaran terlebih dahulu.');
          }
        }catch(error){
          console.error('Error fetching cart data:', error);
        }

      } else {
        alert("Silakan pilih metode pembayaran terlebih dahulu.");
      }
    } catch (error) {
      console.error("Error fetching user data:", error);
      setUserDataError(error);
    }
  };

  const generateRandomInvoiceNumber = () => {
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let invoiceNumber = "WDJHUYY";

    for (let i = 0; i < 4; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      invoiceNumber += characters.charAt(randomIndex);
    }

    invoiceNumber += Math.floor(1000 + Math.random() * 9000);
    return invoiceNumber;
  };

  return (
    <Modal
      open={isOpen}
      onClose={onClose}
      aria-labelledby="payment-modal-title"
      aria-describedby="payment-modal-description"
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: 400,
          bgcolor: "background.paper",
          border: "2px solid #000",
          boxShadow: 24,
          p: 4,
        }}
      >
        <Typography id="payment-modal-title" variant="h6" component="h2">
          Pilih Metode Pembayaran
        </Typography>
        <Typography id="payment-modal-description" sx={{ mt: 2 }}>
          {paymentMethods.map((method) => (
            <Card
              key={method.name}
              sx={{
                backgroundColor:
                  selectedMethod === method.name ? "#6499E9" : "white",
                cursor: "pointer",
                margin: "8px",
                display: "flex",
                alignItems: "center",
              }}
              onClick={() => handleSelectMethod(method.name)}
              style={{ margin: "8px", cursor: "pointer" }}
            >
              <CardContent>
                <img
                  src={`data:image/png;base64,${method.logo}`}
                  alt={method.name}
                  style={{ width: "30px", marginRight: "8px" }}
                />
                {method.name}
              </CardContent>
            </Card>
          ))}
        </Typography>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: "16px",
          }}
        >
          <Button variant="outlined" color="error" onClick={onClose}>
            Cancel
          </Button>
          <Button
            component={Link}
            to="/success-payment"
            variant="contained"
            color="error"
            onClick={handlePaymentComplete}
            sx={{ fontWeight: "bold" }}
          >
            Pay Now
          </Button>
        </div>
      </Box>
    </Modal>
  );
}

export default PaymentModal;
