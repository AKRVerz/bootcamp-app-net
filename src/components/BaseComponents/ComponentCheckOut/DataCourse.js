import React from "react";
import Checkbox from "@mui/material/Checkbox";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";

function DataCourse({ data, onDelete, onCarSelect, merkName, scheduleId }) {
  const apiUrl = process.env.REACT_APP_API_URL;
  const handleCheckboxChange = () => {
    const isSelected = !data.isChecked;
    if (onCarSelect) {
      onCarSelect(data.id, isSelected);
    }
  };

  const handleDeleteClick = () => {
    // Kirim permintaan DELETE ke API dengan ID cart sebagai parameter
    fetch(`${apiUrl}/api/Cart/${data.id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        // Mungkin Anda perlu menambahkan header otorisasi di sini jika diperlukan
      },
      body: JSON.stringify([data.id]), // Mengirimkan ID cart sebagai array
    })
      .then((response) => {
        if (response.ok) {
          // Panggil fungsi onDelete jika permintaan berhasil
          onDelete(data.id);
        } else {
          // Handle kesalahan jika diperlukan
          console.error('Gagal menghapus item dari keranjang.');
        }
      })
      .catch((error) => {
        // Handle kesalahan jaringan jika diperlukan
        console.error('Terjadi kesalahan jaringan:', error);
      });
  };

  return (
    <Grid
      container
      justifyContent="space-between"
      alignItems="center"
      spacing={2}
      style={{
        borderTop: '1px solid #ccc',
        padding: '8px',
        marginBottom: '8px',
        marginTop: '8px',
      }}
    >
      <Grid item xs={12} sm={12} md={6}>
        <div style={{ display: 'flex', alignItems: 'center', padding: '8px' }}>
          <Checkbox
            checked={data.isChecked || false}
            onChange={handleCheckboxChange}
            inputProps={{ 'aria-label': 'Select this car' }}
          />
          <img
            src={`data:image/png;base64,${data.imagePath}`}
            alt={data.title}
            style={{
              marginRight: '8px',
              width: '100%',
              maxWidth: '200px',
              height: 'auto',
            }}
          />
          <div>
            <Typography variant="body2" sx={{ opacity: 0.7 }}>
              {merkName} {/* Tampilkan nama merk */}
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ fontWeight: 'bold', fontSize: '1.25rem' }}
            >
              {data.title}
            </Typography>
            <Typography variant="body2" sx={{ opacity: 0.9 }}>
              {data.jadwal}
            </Typography>
            <Typography
              variant="body2"
              sx={{ fontWeight: 'bold', color: 'red', fontSize: '1rem' }}
            >
              IDR. {data.price}
            </Typography>
            <Typography variant="body2">
              Schedule ID: {scheduleId} {/* Tampilkan scheduleId */}
            </Typography>
          </div>
        </div>
      </Grid>
      <Grid item xs={12} sm={12} md={6}>
        <div style={{ textAlign: 'right' }}>
          <IconButton
            aria-label="delete"
            onClick={handleDeleteClick}
            color="primary"
          >
            <DeleteIcon />
          </IconButton>
        </div>
      </Grid>
    </Grid>
  );
}

export default DataCourse;
