import React, { useState, useEffect } from 'react';
import DataCourse from './DataCourse';
import TotalPayment from './TotalPayment';
import Container from '@mui/material/Container';
import axios from 'axios';
import Cookies from 'js-cookie';
import jwt_decode from 'jwt-decode';

const MainCheckoutPage = () => {
  const [cars, setCars] = useState([]);
  const [categories, setCategories] = useState([]);
  const [userId, setUserId] = useState(null);
  const [userDataLoading, setUserDataLoading] = useState(true);
  const [userDataError, setUserDataError] = useState(null);
  const [selectedCards, setSelectedCards] = useState([]);
  const apiUrl = process.env.REACT_APP_API_URL;
  // const [cart, setCart] = useState([]);

  // useEffect(() => {
  //   fetch("https://localhost:7040/api/Cart")
  //     .then((response) => response.json())
  //     // 4. Setting *dogImage* to the image url that we received from the response above
  //     .then((data) => setCart(data.data));
  //   console.log(cart);
  // }, []);

  useEffect(() => {
    // Mendapatkan token dari cookies
    const token = Cookies.get('payload');
    console.log('Token:', token);

    // Mendekode token untuk mendapatkan username
    const decodedToken = jwt_decode(token);
    console.log('Decoded Token:', decodedToken);

    if (!token) {
      console.error('Token tidak tersedia.');
      return;
    }

    const username = decodedToken.name;
    console.log(username);

    if (!username) {
      console.error('ID pengguna tidak valid.');
      return;
    }

    // Set userId menggunakan fetch data dari API
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${apiUrl}/api/User/GetByUsername/${username}`
        );

        console.log('Response from API:', response);

        const userData = response.data;
        console.log('User Data:', userData);

        console.log('User ID:', userData.id);

        setUserId(userData.id);
        setUserDataLoading(false);
      } catch (error) {
        console.error('Error fetching user data:', error);
        setUserDataError(error);
        setUserDataLoading(false);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchCartData = async () => {
      const apiUrl = process.env.REACT_APP_API_URL;

      try {
        const cartResponse = await axios.get(`${apiUrl}/api/Cart/${userId}`);
        setCars(cartResponse.data);

        const categoriesResponse = await axios.get(`${apiUrl}/api/Category`);
        setCategories(categoriesResponse.data);
      } catch (error) {
        console.error('Error fetching cart data from API:', error);
      }
    };

    if (userId !== null) {
      fetchCartData();
    }
  }, [userId]);

  const handleDeleteCar = (carId) => {
    setCars(cars.filter((car) => car.id !== carId));
  };

  const handleCarSelect = (carId, isSelected) => {
    setCars((prevCars) => {
      const updatedCars = prevCars.map((car) =>
        car.id === carId ? { ...car, isChecked: isSelected } : car
      );

      if (isSelected) {
        setSelectedCards((prevSelectedCards) => [...prevSelectedCards, carId]);
      } else {
        setSelectedCards((prevSelectedCards) =>
          prevSelectedCards.filter((id) => id !== carId)
        );
      }

      return updatedCars;
    });
  };

  const calculateTotal = () => {
    return cars.reduce(
      (acc, car) => (car.isChecked ? acc + car.price : acc),
      0
    );
  };

  const total = calculateTotal();

  return (
    <Container>
      <h1>Daftar Mobil</h1>
      <div
        style={{
          maxHeight: '600px',
          overflowY: 'auto',
        }}
      >
        {userDataLoading ? (
          <div>Loading user data...</div>
        ) : userDataError ? (
          <div>Error loading user data: {userDataError.message}</div>
        ) : (
          cars.map((car) => {
            const category = categories.find(
              (category) => category.id === car.merkId
            );
            const merkName = category
              ? category.categoryName
              : 'Merk tidak ditemukan';

            return (
              <DataCourse
                key={car.id}
                data={car}
                onDelete={handleDeleteCar}
                onCarSelect={handleCarSelect}
                merkName={merkName}
              />
            );
          })
        )}
        <TotalPayment total={total} />
      </div>
    </Container>
  );
};

export default MainCheckoutPage;
