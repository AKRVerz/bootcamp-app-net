// src/components/TotalPayment.js
import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import PaymentModal from './PaymentModal'; // Import komponen modal pembayaran

function TotalPayment({ total }) {
  const [isModalOpen, setIsModalOpen] = useState(false); // State untuk mengontrol modal

  // Fungsi untuk membuka modal
  const openModal = () => {
    setIsModalOpen(true);
  };

  // Fungsi untuk menutup modal
  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div>
      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        style={{
          position: 'fixed',
          bottom: '0',
          left: '0',
          right: '0',
          padding: '16px',
          background: 'white',
          borderTop: '1px solid #ccc',
          maxWidth: '1038px',
          margin: '0 auto',
        }}
      >
        <Grid item>
          <span style={{ fontWeight: 'bold', fontSize: '1.2rem' }}>
            Total Price{' '}
            <span style={{ color: '#FF0000', fontSize: '1.2rem' }}>
              IDR {total}
            </span>
          </span>
        </Grid>
        <Grid item>
          <Button
            style={{}}
            variant="contained"
            color="error"
            sx={{ fontWeight: 'bold' }}
            onClick={openModal}
          >
            Pay Now
          </Button>
        </Grid>
      </Grid>

      {/* Tampilkan modal jika isModalOpen true */}
      <PaymentModal isOpen={isModalOpen} onClose={closeModal} />
    </div>
  );
}

export default TotalPayment;
