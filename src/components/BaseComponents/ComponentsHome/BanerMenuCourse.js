// BanerMenuCourse.js
import React from 'react';
import { useParams } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { useMediaQuery } from '@mui/material';
import useCarTypesById from '../../../hooks/useCarTypesById';

const BanerMenuCourse = () => {
  const isMobile = useMediaQuery('(max-width:600px)');
  const { id } = useParams();
  const { car, loading, error } = useCarTypesById(id);
  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!car) {
    return <div>Car not found</div>;
  }

  return (
    <div>
      <Box
        sx={{
          backgroundImage: `url('https://i.ibb.co/s3wkzgp/image-3-1.png')`, // Replace with your image path
          backgroundSize: isMobile ? 'contain' : '100% 720px',
          backgroundRepeat: 'no-repeat',
          width: '100%',
          height: '420px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      ></Box>
      <Typography
        variant="h4"
        style={{
          color: 'grey',
          marginBottom: '20px',
          marginTop: '20px',
          marginLeft: '20px',
          textAlign: 'left',
        }}
      >
        {car.categoryName}
        {console.log('env', process.env.REACT_APP_API_URL)}
      </Typography>
      <Typography
        variant="p"
        style={{
          color: 'black',
          marginBottom: '20px',
          marginTop: '20px',
          marginLeft: '20px',
          textAlign: 'left',
          display: 'flex',
          justifyContent: 'left',
        }}
      >
        {car.deskripsi}
      </Typography>
      {/* <Typography
        variant="p"
        style={{
          color: 'black',
          marginBottom: '20px',
          marginTop: '20px',
          marginLeft: '20px',
          textAlign: 'left',
          display: 'flex',
          justifyContent: 'left',
        }}
      >
        {car.description}
      </Typography> */}
    </div>
  );
};

export default BanerMenuCourse;
