import React from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import useMediaQuery from '@mui/material/useMediaQuery';
import ListCardComponent from './ListCardComponent'; // Import komponen ListCardComponent
import useCarsDataIsActive from '../../../hooks/useCarsDataIsActive';

const CardCarComponent = ({ title }) => {
  const isMobile = useMediaQuery('(max-width:600px)');
  const { carsData, loading, error } = useCarsDataIsActive();

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  const limitedCarsData = carsData.slice(0, 5);
  console.log(carsData)
  return (
    <div>
      <Typography
        variant="h4"
        style={{
          color: '#790B0A',
          marginBottom: '20px',
          marginTop: '20px',
          textAlign: 'center',
        }}
      >
        {title}
      </Typography>
      <Box
        sx={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          padding: '10px',
        }}
      >
        <ListCardComponent
          limitedCarsData={limitedCarsData}
          isMobile={isMobile}
        />
      </Box>
    </div>
  );
};

export default CardCarComponent;
