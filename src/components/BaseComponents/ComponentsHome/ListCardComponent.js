import React from "react";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { Typography } from "@mui/material";

const ListCardComponent = ({ limitedCarsData, isMobile }) => {
  return (
    <Grid container spacing={isMobile ? 1 : 2}>
      {limitedCarsData.map((card, index) => (
        <Grid item xs={12} sm={6} md={4} key={index}>
          <Card>
            <CardMedia
              component="img"
              height={isMobile ? '200' : '350'}
              src={`data:image/png;base64,${card.imagePath}`}
              alt={card.title}
              sx={{ objectFit: 'contain' }}
            />
            <CardContent>
              <Typography variant="body2" style={{ opacity: 0.7 }}>
                {card.categoryName}
              </Typography>
              <Typography
                variant={isMobile ? 'h6' : 'h5'}
                component="div"
                style={{
                  color: 'black',
                }}
              >
                <Link
                  style={{ textDecoration: 'none', color: 'black' }}
                  to={`/myclass/${card.id}`}
                >
                  {card.title}
                </Link>
              </Typography>
              <Typography
                variant="body2"
                style={{
                  fontWeight: 'bold',
                  color: '#790B0A',
                  marginTop: '20px',
                  fontSize: isMobile ? '16px' : '20px',
                }}
              >
                IDR.{card.price}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

ListCardComponent.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  limitedCarsData: PropTypes.array.isRequired,
};

export default ListCardComponent;
