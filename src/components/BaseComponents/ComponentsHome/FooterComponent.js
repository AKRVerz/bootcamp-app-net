import React from 'react';
import {
  Box,
  Typography,
  Grid,
  IconButton,
  useMediaQuery,
} from '@mui/material';
import InstagramIcon from '@mui/icons-material/Instagram';
import PhoneIcon from '@mui/icons-material/Phone';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TelegramIcon from '@mui/icons-material/Telegram';
import EmailIcon from '@mui/icons-material/Email';

const FooterComponent = () => {
  const isMobile = useMediaQuery('(max-width:600px)');
  return (
    <div style={{ overflowX: 'hidden' }}>
      <Box
        color="white"
        padding="20px"
        display="flex"
        flexDirection={isMobile ? 'column' : 'row'}
        justifyContent="space-between"
        alignItems="flex-start"
        flexGrow="1"
      >
        {/* Baris 1 - Column Pertama */}
        <Box sx={{ width: '228px', margin: '50px' }}>
          {/* Isi konten untuk column pertama */}
          <Typography
            variant="h6"
            sx={{
              fontFamily: 'Poppins',
              fontSize: '24px',
              fontWeight: 500,
              color: '#790B0A',
            }}
          >
            About Us
          </Typography>
          <Typography
            variant="body1"
            sx={{ fontFamily: 'Poppins', fontSize: '14px', color: 'black' }}
          >
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
            quae ab illo inventore veritatis et quasi architecto beatae vitae
            dicta sunt explicabo.
          </Typography>
        </Box>

        {/* Baris 2 - Column Kedua */}
        <Box sx={{ margin: '50px', width: '228px' }}>
          {/* Isi konten atau komponen untuk column kedua di sini */}
          <Typography
            variant="h6"
            sx={{
              fontFamily: 'Poppins',
              fontSize: '24px',
              fontWeight: 500,
              color: '#790B0A',
              marginBottom: '10px',
            }}
          >
            Products
          </Typography>
          <Grid container spacing={0}>
            {/* Daftar produk bagian kiri */}
            <Grid item xs={6}>
              <Typography
                variant="body1"
                sx={{ fontFamily: 'Poppins', fontSize: '14px', color: 'black' }}
              >
                - Electric
                <br />
                - LCGC
                <br />
                - Offroad
                <br />
                - SUV
                <br />
              </Typography>
            </Grid>
            {/* Daftar produk bagian kanan */}
            <Grid item xs={6}>
              <Typography
                variant="body1"
                sx={{ fontFamily: 'Poppins', fontSize: '14px', color: 'black' }}
              >
                - Hatchback
                <br />
                - MPV
                <br />
                - Sedan
                <br />
                - Truck
                <br />
              </Typography>
            </Grid>
          </Grid>
        </Box>

        {/* Baris 3 - Column Ketiga */}
        <Box
          sx={{
            margin: '50px',
            width: '352px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <Grid container spacing={2}>
            {/* Sub-Column Pertama */}
            <Grid>
              {/* Isi konten atau komponen untuk sub-column kedua di sini */}
              <Typography
                variant="h6"
                sx={{
                  fontFamily: 'Poppins',
                  fontSize: '24px',
                  fontWeight: 500,
                  color: '#790B0A',
                }}
              >
                Address
              </Typography>
              <Typography
                variant="body1"
                sx={{
                  fontFamily: 'Poppins',
                  fontSize: '14px',
                  color: 'black',
                  width: '330px',
                }}
              >
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque.
              </Typography>
            </Grid>

            {/* Sub-Column Kedua */}
            <Grid>
              {/* Isi konten atau komponen untuk sub-column kedua di sini */}
              <Typography
                variant="h6"
                sx={{
                  fontFamily: 'Poppins',
                  fontSize: '24px',
                  fontWeight: 500,
                  color: '#790B0A',
                }}
              >
                Contact Us
              </Typography>
              <Box>
                <IconButton
                  href="https://wa.me/your-phonenumber"
                  target="_blank"
                  rel="noopener noreferrer"
                  sx={iconStyles}
                >
                  <PhoneIcon />
                </IconButton>
                <IconButton
                  href="https://www.instagram.com/your-instagram-page"
                  target="_blank"
                  rel="noopener noreferrer"
                  sx={iconStyles}
                >
                  <InstagramIcon />
                </IconButton>
                <IconButton
                  href="https://www.youtube.com/your-channel"
                  target="_blank"
                  rel="noopener noreferrer"
                  sx={iconStyles}
                >
                  <YouTubeIcon />
                </IconButton>
                <IconButton
                  href="https://twitter.com/your-twitter-handle"
                  target="_blank"
                  rel="noopener noreferrer"
                  sx={iconStyles}
                >
                  <TelegramIcon />
                </IconButton>
                <IconButton
                  href="mailto:your-email@example.com"
                  sx={iconStyles}
                >
                  <EmailIcon />
                </IconButton>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </div>
  );
};

const iconStyles = {
  color: 'white',
  width: '48px',
  height: '40px',
  borderRadius: '30px',
  margin: '5px',
  backgroundColor: '#790B0A',
  transition: 'background-color 0.3s ease',
  '&:hover': {
    backgroundColor: '#790B0A',
  },
};

export default FooterComponent;
