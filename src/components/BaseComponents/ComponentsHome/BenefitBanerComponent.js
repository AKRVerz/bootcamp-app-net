import React from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import PropTypes from 'prop-types';

const BenefitBanerComponents = ({ isMobile, title }) => {
  return (
    <Grid>
      <Typography
        variant="h4"
        style={{
          color: '#790B0A',
          textAlign: 'center',
        }}
      >
        {title}
      </Typography>
      <Grid container spacing={2} style={{ marginTop: '20px' }}>
        <Grid>
          <Box
            style={{
              width: isMobile ? '100%' : '706px', // Lebar penuh pada tampilan mobile
              height: isMobile ? 'auto' : '280px', // Tinggi otomatis
              padding: '20px',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
            }}
          >
            <Typography
              variant="body1"
              style={{
                marginTop: '20px',
                fontSize: isMobile ? '14px' : '16px', // Ukuran font berdasarkan tampilan
              }}
            >
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
              aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
              eos qui ratione voluptatem sequi nesciunt.
            </Typography>
            <Typography
              variant="body1"
              style={{
                marginTop: '20px',
                fontSize: isMobile ? '14px' : '16px', // Ukuran font berdasarkan tampilan
              }}
            >
              Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
              consectetur, adipisci velit, sed quia non numquam. Neque porro
              quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
              adipisci velit, sed quia non numquam eius modi tempora incidunt ut
              labore et dolore magnam aliquam quaerat voluptatem.
            </Typography>
          </Box>
        </Grid>
        {/* Sub-Column 2 */}
        <Grid>
          <Box
            style={{
              width: isMobile ? '100%' : '373.33px', // Lebar penuh pada tampilan mobile
              height: isMobile ? 'auto' : '280px', // Tinggi otomatis
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <img
              src="image 9.png" // Ganti dengan URL gambar pertama Anda
              alt="Deskripsi Gambar" // Ganti dengan deskripsi gambar pertama Anda
              style={{
                maxWidth: '100%', // Membuat gambar tidak melebihi lebar konten
                maxHeight: '100%', // Membuat gambar tidak melebihi tinggi konten
              }}
            />
          </Box>
        </Grid>
      </Grid>
    </Grid>
  );
};

BenefitBanerComponents.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
};

export default BenefitBanerComponents;
