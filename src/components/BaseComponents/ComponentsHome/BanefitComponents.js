import React from 'react';
import { Box, Container, Grid, useMediaQuery } from '@mui/material';
import useCarTypes from '../../../hooks/useCarTypes';
import BenefitBanerComponents from './BenefitBanerComponent';
import BenefitCategoryCarsComponents from './BenefitCategoryCarsComponents';
import useCategoriesDataIsActive from '../../../hooks/useCategoryIsActive';

const BenefitComponents = () => {
  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));

  // Gunakan custom hook useCarTypes untuk mengambil data car types
  const { categoriesData, loading, error } = useCategoriesDataIsActive();
  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
  const limitedCarsData = categoriesData.slice(0, 8);
  return (
    <Container>
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="space-between"
        style={{ gap: '20px' }}
      >
        <Grid item xs={12} sm={6}>
          <BenefitBanerComponents
            isMobile={isMobile}
            title="Gets your best benefit"
          />
        </Grid>
        <Box
          style={{
            width: '100%',
            height: 'auto',
          }}
        >
          <Grid item xs={12} sm={6}>
            <BenefitCategoryCarsComponents
              isMobile={isMobile}
              limitedCarsData={limitedCarsData}
            />
          </Grid>
        </Box>
      </Box>
    </Container>
  );
};

export default BenefitComponents;
