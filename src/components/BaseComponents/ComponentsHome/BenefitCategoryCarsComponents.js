import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const BenefitCategoryCarsComponents = ({ isMobile, limitedCarsData }) => {
  return (
    <Box
      style={{
        width: '100%',
        height: 'auto',
      }}
    >
      <Typography
        variant="h4"
        style={{
          color: '#790B0A',
          textAlign: 'center',
        }}
      >
        More car types you can choose
      </Typography>
      <Grid container spacing={2} marginTop="20px">
        {limitedCarsData.map((carType, index) => (
          <Grid item key={index} xs={12} sm={6} md={3}>
            <Card
              style={{
                width: '100%',
                height: '120px',
                backgroundColor: 'transparent',
                boxShadow: 'none',
              }}
            >
              <CardMedia
                component="img"
                height="60"
                src={`data:image/png;base64,${carType.imagePath}`}
                // image="/logo.png"
                alt={carType.categoryName}
                sx={{ objectFit: 'contain' }}
              />
              <CardContent>
                <Typography
                  align="center"
                  fontWeight="bold"
                  sx={{
                    fontSize: isMobile ? '12px' : '16px',
                  }}
                >
                  <Link
                    style={{ textDecoration: 'none', color: 'black' }}
                    to={`/menu-course/${carType.id}`}
                  >
                    {carType.categoryName}
                  </Link>
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

BenefitCategoryCarsComponents.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  limitedCarsData: PropTypes.array.isRequired,
};

export default BenefitCategoryCarsComponents;
