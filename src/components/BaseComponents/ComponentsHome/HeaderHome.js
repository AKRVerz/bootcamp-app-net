import React, { useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PersonIcon from '@mui/icons-material/Person';
import Button from '@mui/material/Button';
import Collapse from '@mui/material/Collapse';
import { useMediaQuery } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import LogoutIcon from '@mui/icons-material/Logout';
import ToggleTheme from '../../Temp/ToggleTheme';
import Cookies from 'js-cookie';

const HeaderHome = ({ showButtons }) => {
  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const [isMenuOpen, setMenuOpen] = useState(false);
  const navigate = useNavigate(); // Use useNavigate for redirection

  const toggleMenu = () => {
    setMenuOpen(!isMenuOpen);
  };

  const goToCheckout = () => {
    navigate('/checkout'); // Use navigate to navigate
  };

  const goToLoginPage = () => {
    navigate('/login'); // Use navigate to navigate
  };
  const goToInvoicePage = () => {
    navigate('/invoice'); //
  };
  const goToClassCourse = () => {
    navigate('/class-course');
  }

  const handleLogout = () => {
    // Clear the token cookie
    Cookies.remove('payload');

    // Redirect to the login page
    navigate('/login');
  };

  return (
    <AppBar
      position="static"
      elevation={0}
      sx={{ backgroundColor: 'white', padding: '8px 50px' }}
    >
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Link to="/" style={{ textDecoration: 'none', color: 'black' }}>
          <Typography
            variant="h6"
            sx={{
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <img
              src="/logo.png"
              alt="Logo"
              style={{ marginRight: '8px', width: '40px', height: '40px' }}
            />
            Otomobil
          </Typography>
        </Link>
        <ToggleTheme />

        <div style={{ position: 'relative' }}>
          {isMobile ? (
            <>
              {showButtons && (
                <Button
                  style={{
                    color: '#790B0A',
                    marginBottom: '10px',
                  }}
                  onClick={toggleMenu}
                >
                  <i className="material-icons">menu</i>
                </Button>
              )}
              <Collapse
                in={isMenuOpen}
                style={{
                  position: 'absolute',
                  top: '100%',
                  left: 0,
                  backgroundColor: 'white',
                  zIndex: 1,
                  boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
                }}
              >
                {showButtons && (
                  <>
                    <Button
                      style={{
                        color: '#790B0A',
                        marginRight: '10px',
                      }}
                      onClick={goToCheckout}
                    >
                      <ShoppingCartIcon onClick={goToCheckout} />
                    </Button>
                    <Button
                      style={{
                        color: '#790B0A',
                        marginRight: '10px',
                      }}
                      onClick={goToClassCourse}
                    >
                      MyClass
                    </Button>
                    <Button
                      style={{
                        color: '#790B0A',
                        marginRight: '10px',
                      }}
                      onClick={goToInvoicePage}
                    >
                      Invoice
                    </Button>
                    <Button
                      style={{
                        color: 'black',
                        marginRight: '10px',
                      }}
                      onClick={goToLoginPage}
                    >
                      |
                    </Button>
                    <Button
                      style={{
                        color: '#790B0A',
                        marginRight: '10px',
                      }}
                    >
                      <PersonIcon />
                    </Button>
                    <Button
                      style={{
                        color: 'black',
                        marginRight: '10px',
                      }}
                      onClick={handleLogout} // Call handleLogout function here
                    >
                      <LogoutIcon />
                    </Button>
                  </>
                )}
              </Collapse>
            </>
          ) : (
            <>
              {showButtons && (
                <>
                  <Button
                    style={{
                      color: '#790B0A',
                      marginRight: '10px',
                    }}
                    onClick={goToCheckout} // Buka/tutup CartPopup saat tombol diklik
                  >
                    <ShoppingCartIcon />
                  </Button>
                  <Button
                    style={{
                      color: '#790B0A',
                      marginRight: '10px',
                    }}
                    onClick={goToClassCourse}
                  >
                    MyClass
                  </Button>
                  <Button
                    style={{
                      color: '#790B0A',
                      marginRight: '10px',
                    }}
                    onClick={goToInvoicePage}
                  >
                    Invoice
                  </Button>
                  <Button
                    style={{
                      color: 'black',
                      marginRight: '10px',
                    }}
                    onClick={goToLoginPage}
                  >
                    |
                  </Button>
                  <Button
                    style={{
                      color: '#790B0A',
                      marginRight: '10px',
                    }}
                  >
                    <PersonIcon />
                  </Button>
                  <Button
                    style={{
                      color: 'black',
                      marginRight: '10px',
                    }}
                    onClick={handleLogout} // Call handleLogout function here
                  >
                    <LogoutIcon />
                  </Button>
                </>
              )}
            </>
          )}
        </div>
      </div>
    </AppBar>
  );
};

export default HeaderHome;
