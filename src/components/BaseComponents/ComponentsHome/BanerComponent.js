import React from 'react';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { useMediaQuery } from '@mui/material';

const BanerComponent = () => {
  const isMobile = useMediaQuery('(max-width:600px)');

  return (
    <div>
      <Box
        sx={{
          backgroundImage: 'url("image 3.png")',
          backgroundSize: isMobile ? 'cover' : '100% 720px',
          backgroundRepeat: 'no-repeat',
          width: '100%',
          height: '720px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Paper
          elevation={0}
          sx={{
            backgroundColor: 'rgba(0, 0, 0, 0)',
            padding: isMobile ? '40px' : '120px',
            textAlign: 'center',
            color: 'white',
            borderRadius: '0',
          }}
        >
          <Typography
            variant="h3"
            fontSize={isMobile ? '25px' : '52px'}
            padding="20px"
          >
            We provide driving lessons for various types of cars
          </Typography>
          <Typography fontSize={isMobile ? '15px' : '25px'}>
            Professional staff who are ready to help you to become a much-needed
            reliable driver
          </Typography>
        </Paper>
        <Box
          sx={{
            display: 'flex',
            flexDirection: isMobile ? 'column' : 'row', // Mengatur flex direction sesuai dengan layar
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Paper
            elevation={0} // Menghapus shadow dengan mengatur elevation menjadi 0
            style={{
              backgroundColor: 'rgba(0, 0, 0, 0)', // Warna latar belakang overlay
              padding: '20px',
              textAlign: 'center',
              color: 'white',
              borderRadius: '0', // Menghapus radius background
            }}
          >
            <Typography variant="h3">50+</Typography>
            <Typography variant="body1">
              A class ready to make you a reliable driver
            </Typography>
          </Paper>

          <Paper
            elevation={0} // Menghapus shadow dengan mengatur elevation menjadi 0
            style={{
              backgroundColor: 'rgba(0, 0, 0, 0)', // Warna latar belakang overlay
              padding: '20px',
              textAlign: 'center',
              color: 'white',
              borderRadius: '0', // Menghapus radius background
            }}
          >
            <Typography variant="h3">20+</Typography>
            <Typography variant="body1">
              Professional workforce with great experience
            </Typography>
          </Paper>
          <Paper
            elevation={0} // Menghapus shadow dengan mengatur elevation menjadi 0
            style={{
              backgroundColor: 'rgba(0, 0, 0, 0)', // Warna latar belakang overlay
              padding: '20px',
              textAlign: 'center',
              color: 'white',
              borderRadius: '0', // Menghapus radius background
            }}
          >
            <Typography variant="h3">10+</Typography>
            <Typography variant="body1">
              Cooperate with driver service partners
            </Typography>
          </Paper>
        </Box>
      </Box>
    </div>
  );
};

export default BanerComponent;
