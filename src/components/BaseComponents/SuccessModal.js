import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
  DialogActions,
  Button,
} from '@mui/material';

const SuccessModal = ({ open, onClose, onGoToLogin }) => {
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Registration Successful</DialogTitle>
      <DialogContent>
        <Typography variant="body1">
          Your account has been created successfully. <br /> Please check your
          email for confirmation.
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={onGoToLogin} color="primary">
          Go to Login
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SuccessModal;
