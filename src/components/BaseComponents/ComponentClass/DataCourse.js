import React from "react";

import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";

function DataCourse({ data, onDelete, onCarSelect, merkName }) {
  const handleCheckboxChange = () => {
    const isSelected = !data.isChecked;
    if (onCarSelect) {
      onCarSelect(data.id, isSelected);
    }
  };

  const handleDeleteClick = () => {
    onDelete(data.id);
  };

  return (
    <Grid
      container
      justifyContent="space-between"
      alignItems="center"
      spacing={2}
      style={{
        borderTop: '1px solid #ccc',
        padding: '8px',
        marginBottom: '8px',
        marginTop: '8px',
      }}
    >
      <Grid item xs={12} sm={12} md={6}>
        <div style={{ display: 'flex', alignItems: 'center', padding: '8px' }}>
          <img
            src={`data:image/png;base64,${data.imagePath}`}
            alt={data.title}
            style={{
              marginRight: '8px',
              width: '100%',
              maxWidth: '200px',
              height: 'auto',
            }}
          />
          <div>
            <Typography variant="body2" sx={{ opacity: 0.7 }}>
              {data.category} {/* Tampilkan nama merk */}
            </Typography>
            <Typography
              variant="subtitle1"
              sx={{ fontWeight: 'bold', fontSize: '1.25rem' }}
            >
              {data.title}
            </Typography>
            <Typography
              variant="body2"
              sx={{ opacity: 0.9, color: '#790B0A', fontWeight: '600' }}
            >
              Schedule : {data.schedule}
            </Typography>
          </div>
        </div>
      </Grid>
    </Grid>
  );
}

export default DataCourse;
