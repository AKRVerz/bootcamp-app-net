import React, { useEffect, useState } from "react";
import Card from "react-bootstrap/Card";
import {
  CardMedia,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Button,
  Box,
  CardContent,
} from "@mui/material";
import useCarsDataById from "../../../hooks/useCarDataById";
import useScheduleById from "../../../hooks/useScheduleById";
import { useParams, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import jwt_decode from "jwt-decode";
import axios from "axios";
import Popup from "../Popup";

const ListCarComponent = () => {
  const { id } = useParams();
  const [userId, setUserId] = useState(null);
  const [userDataLoading, setUserDataLoading] = useState(true);
  const [userDataError, setUserDataError] = useState(null);
  const [isPopupOpen, setPopupOpen] = useState(false);
  const [popupMessage, setPopupMessage] = useState("");
  const [isPopupSuccess, setPopupSuccess] = useState(false);
  const navigate = useNavigate();
  const apiUrl = process.env.REACT_APP_API_URL;

  const { car, loading: carLoading, error: carError } = useCarsDataById(id);

  const {
    scheduleData,
    loading: scheduleLoading,
    error: scheduleError,
  } = useScheduleById(id);

  const [selectedSchedule, setSelectedSchedule] = useState("");

  const BuyNow = () => {
    handleAddToCart();
    navigate('/checkout');
  };

  useEffect(() => {
    if (car) {
      console.log("Data Mobil:", car);
    }
    if (scheduleData) {
      console.log("Data Jadwal:", scheduleData);
      setSelectedSchedule(scheduleData[0]?.jadwal || "");
    }
    if (carError) {
      console.error("Kesalahan Mobil:", carError);
    }
    if (scheduleError) {
      console.error("Kesalahan Jadwal:", scheduleError);
    }
  }, [car, scheduleData, carError, scheduleError]);

  const handleScheduleChange = (event) => {
    setSelectedSchedule(event.target.value);
  };

  // const handleAddToCart = () => {
  //   const token = Cookies.get('payload');

  //   // Mendekode token untuk mendapatkan username
  //   const decodedToken = jwt_decode(token);
  //   const username = decodedToken.name;

  //   if (!token) {
  //     console.error('Token tidak tersedia.');
  //     return;
  //   }

  //   if (!username) {
  //     console.error('ID pengguna tidak valid.');
  //     return;
  //   }

  //   const fetchData = async () => {
  //     try {
  //       const response = await axios.get(
  //         `${apiUrly}/api/User/GetByUsername/${username}`
  //       );

  //       console.log('Response from API:', response);

  //       const userData = response.data;
  //       console.log('User Data:', userData);

  //       console.log('User ID:', userData.id);

  //       setUserId(userData.id);
  //       console.log(userId);
  //       setUserDataLoading(false);
  //     } catch (error) {
  //       console.error('Error fetching user data:', error);
  //       setUserDataError(error);
  //       setUserDataLoading(false);
  //     }
  //   };
  //   fetchData();

  //   const selectedScheduleData = scheduleData.find(
  //     (schedule) => schedule.jadwal === selectedSchedule
  //   );

  //   if (!selectedScheduleData) {
  //     console.error('Jadwal tidak valid.');
  //     return;
  //   }

  //   const selectedScheduleId = selectedScheduleData.id;
  //   console.log('ID Schedule yang Dipilih:', selectedScheduleId);

  //   const data = {
  //     id_User: userId,
  //     id_Schedule: selectedScheduleId,
  //   };
  //   console.log(data);
  //   const addToCartUrl = `${apiUrly}/api/Cart`;

  //   fetch(addToCartUrl, {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: JSON.stringify(data),
  //   })
  //     .then((response) => response.json())
  //     .then((result) => {
  //       console.log('Item berhasil ditambahkan ke keranjang:', result);
  //       setPopupMessage('Item berhasil ditambahkan ke keranjang.');
  //       setPopupSuccess(true);
  //       setPopupOpen(true);
  //     })
  //     .catch((error) => {
  //       console.error(
  //         'Terjadi kesalahan saat menambahkan item ke keranjang:',
  //         error
  //       );
  //       setPopupMessage(
  //         'Terjadi kesalahan saat menambahkan item ke keranjang.'
  //       );
  //       setPopupSuccess(false);
  //       setPopupOpen(true);
  //     });
  // };
  const handleAddToCart = async () => {
    const token = Cookies.get("payload");
    const decodedToken = jwt_decode(token);
    const username = decodedToken.name;

    if (!token || !username) {
      console.log("Token atau ID pengguna tidak valid.");
      return;
    }

    try {
      const response = await axios.get(
        `${apiUrl}/api/User/GetByUsername/${username}`,
      );
      console.log("Response from API:", response);
      const userData = response.data;
      console.log("User Data:", userData);
      console.log("User ID:", userData.id);
      setUserId(userData.id);
      console.log(userData.id);

      const selectedScheduleData = scheduleData.find(
        (schedule) => schedule.jadwal === selectedSchedule,
      );

      if (!selectedScheduleData) {
        console.error("Jadwal tidak valid.");
        return;
      }

      const selectedScheduleId = selectedScheduleData.id;
      console.log("ID Schedule yang Dipilih:", selectedScheduleId);

      const data = {
        id_User: userData.id,
        id_Schedule: selectedScheduleId,
      };
      console.log(data);
      const addToCartUrl = `${apiUrl}/api/Cart`;
      fetch(addToCartUrl, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((result) => {
          console.log("Item berhasil ditambahkan ke keranjang:", result);
          setPopupMessage("Item berhasil ditambahkan ke keranjang.");
          setPopupSuccess(true);
          setPopupOpen(true);
        })
        .catch((error) => {
          console.error(
            "Terjadi kesalahan saat menambahkan item ke keranjang:",
            error,
          );
          setPopupMessage(
            "Terjadi kesalahan saat menambahkan item ke keranjang.",
          );
          setPopupSuccess(false);
          setPopupOpen(true);
        });
    } catch (error) {
      console.error("Error fetching user data:", error);
      setUserDataError(error);
      setUserDataLoading(false);
    }
  };

  return (
    <div className="app-container">
      <div className="list-container">
        <div className="list-item">
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <div style={{ flex: 1 }}>
              {car ? (
                <CardMedia
                  component="img"
                  src={`data:image/png;base64,${car.imagePath}`}
                  alt={car.title}
                  sx={{ objectFit: 'contain' }}
                  style={{ maxWidth: '100%' }}
                />
              ) : (
                <p>Loading car data...</p>
              )}
            </div>
            <div style={{ flex: 1 }}>
              <Card style={{ display: 'flex', flexDirection: 'column' }}>
                <CardContent>
                  <h1>{car ? car.title : 'Loading...'}</h1>
                  <FormControl variant="outlined" fullWidth>
                    <InputLabel>Select Schedule</InputLabel>
                    <Select
                      label="Select a Schedule"
                      value={selectedSchedule}
                      onChange={handleScheduleChange}
                    >
                      {scheduleData.map((schedule, index) => (
                        <MenuItem key={index} value={schedule.jadwal}>
                          {schedule.jadwal}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <div>
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        margin: '10px 0px',
                      }}
                    >
                      <Button
                        variant="outlined"
                        color="error"
                        onClick={handleAddToCart}
                      >
                        Add to Cart
                      </Button>
                      <Button
                        variant="contained"
                        color="error"
                        onClick={BuyNow}
                      >
                        Buy Now
                      </Button>
                    </Box>
                  </div>
                </CardContent>
              </Card>
            </div>
          </div>
          <hr style={{ color: 'black', backgroundColor: 'black' }} />
          <div>
            <p>{car ? car.description : 'Loading...'}</p>
          </div>
        </div>
      </div>
      <Popup
        open={isPopupOpen}
        message={popupMessage}
        onClose={() => setPopupOpen(false)}
        isSuccess={isPopupSuccess}
      />
    </div>
  );
};

export default ListCarComponent;
