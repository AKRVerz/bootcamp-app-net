import React, { useState, useEffect } from 'react';
import DataCourse from './DataCourse';
import Container from '@mui/material/Container';
import axios from 'axios';
import Cookies from 'js-cookie';
import jwt_decode from 'jwt-decode';

const Classuser = () => {
  const [cars, setCars] = useState([]);
  const [categories, setCategories] = useState([]);
  const [userId, setUserId] = useState(null);
  const [userDataLoading, setUserDataLoading] = useState(true);
  const [userDataError, setUserDataError] = useState(null);
  const apiUrl = process.env.REACT_APP_API_URL;

  useEffect(() => {
    // Mendapatkan token dari cookies
    const token = Cookies.get('payload');
    console.log('Token:', token);

    // Mendekode token untuk mendapatkan username
    const decodedToken = jwt_decode(token);
    console.log('Decoded Token:', decodedToken);

    if (!token) {
      console.error('Token tidak tersedia.');
      return;
    }

    const username = decodedToken.name;
    console.log(username);

    if (!username) {
      console.error('ID pengguna tidak valid.');
      return;
    }

    // Set userId menggunakan fetch data dari API
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${apiUrl}/api/User/GetByUsername/${username}`
        );

        console.log('Response from API:', response);

        const userData = response.data;
        console.log('User Data:', userData);

        console.log('User ID:', userData.id);

        setUserId(userData.id);
        setUserDataLoading(false);
      } catch (error) {
        console.error('Error fetching user data:', error);
        setUserDataError(error);
        setUserDataLoading(false);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchCartData = async () => {
      const apiUrl = process.env.REACT_APP_API_URL;

      try {
        const cartResponse = await axios.get(`${apiUrl}/api/User/getAllCourseById/${userId}`);
        setCars(cartResponse.data);

        const categoriesResponse = await axios.get(`${apiUrl}/api/Category`);
        setCategories(categoriesResponse.data);
      } catch (error) {
        console.error('Error fetching cart data from API:', error);
      }
    };

    if (userId !== null) {
      fetchCartData();
    }
  }, [userId]);

  const handleDeleteCar = (carId) => {
    setCars(cars.filter((car) => car.id !== carId));
  };


  return (
    <Container>
      <h1>Daftar Mobil</h1>
      <div
        style={{
          maxHeight: '600px',
          overflowY: 'auto',
        }}
      >
        {userDataLoading ? (
          <div>Loading user data...</div>
        ) : userDataError ? (
          <div>Error loading user data: {userDataError.message}</div>
        ) : (
          cars.map((car) => {
            return (
              <DataCourse
                key={car.id}
                data={car}
                onDelete={handleDeleteCar}
              />
            );
          })
        )}
      </div>
    </Container>
  );
};

export default Classuser;
