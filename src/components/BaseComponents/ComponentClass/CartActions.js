import React from 'react';
import Button from '@mui/material/Button';

const CartActions = ({ onAddToCart, onBuyNow }) => {
  return (
    <div>
      <Button variant="contained" color="primary" onClick={onAddToCart}>
        Add to Cart
      </Button>
      <Button variant="contained" color="secondary" onClick={onBuyNow}>
        Buy Now
      </Button>
    </div>
  );
};

export default CartActions;
