import React, { useEffect, useState } from 'react';
import {
  TextField,
  Button,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  Snackbar,
} from '@mui/material';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';

const EditScheduleComponent = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [jadwal, setJadwal] = useState('');
  const [courseFK, setCourseFK] = useState('');
  const [courses, setCourses] = useState([]);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const apiUrl = process.env.REACT_APP_API_URL;

  useEffect(() => {
    // Mengambil data jadwal dari API berdasarkan id saat komponen dimuat
    axios
      .get(`${apiUrl}/api/Schedule/${id}`)
      .then((response) => {
        const scheduleData = response.data;
        setJadwal(scheduleData.jadwal);
        setCourseFK(scheduleData.courseFK);
      })
      .catch((error) => {
        console.log('Error Fetching Schedule Data: ', error);
      });

    // Mengambil data course untuk opsi pemilihan course
    axios
      .get(`${apiUrl}/api/Course`)
      .then((response) => {
        setCourses(response.data);
      })
      .catch((error) => {
        console.log('Error Fetching Course Data: ', error);
      });
  }, [id]);

  const handleSubmit = (e) => {
    e.preventDefault();

    const payload = {
      jadwal: jadwal,
      courseFK: courseFK,
    };

    axios
      .put(`${apiUrl}/api/Schedule/${id}`, payload)
      .then((response) => {
        console.log('Schedule updated successfully:', response.data);
        setSnackbarOpen(true);

        setTimeout(() => {
          navigate('/adminschedule');
        }, 2000);
      })
      .catch((error) => {
        console.error('Error updating schedule:', error);
      });
  };

  return (
    <div>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <h2>Edit Schedule</h2>
        <TextField
          label="Tanggal"
          required
          variant="outlined"
          color="secondary"
          type="date"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={jadwal}
          onChange={(e) => setJadwal(e.target.value)}
        />
        <FormControl fullWidth variant="outlined" style={{ marginTop: '16px' }}>
          <InputLabel id="courseFK-label">Course</InputLabel>
          <Select
            labelId="courseFK-label"
            label="Course"
            value={courseFK}
            onChange={(e) => setCourseFK(e.target.value)}
          >
            {courses.map((course) => (
              <MenuItem key={course.id} value={course.id}>
                {course.title}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          style={{ marginTop: '16px' }}
        >
          Save
        </Button>
      </form>

      {/* Snackbar for notification */}
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message="Data berhasil diubah!"
        sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF' }}
      />
    </div>
  );
};

export default EditScheduleComponent;
