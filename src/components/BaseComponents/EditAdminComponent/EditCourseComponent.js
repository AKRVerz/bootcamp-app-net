  import React, { useState, useEffect } from 'react';
  import axios from 'axios';
  import Select from 'react-select';
  import { TextField, Button, Snackbar, RadioGroup, FormControlLabel, Radio, Box } from '@mui/material';
  import { useNavigate, useParams } from 'react-router-dom';

  const EditCourseComponent = () => {
    const { id } = useParams();
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [selectedKategori, setSelectedKategori] = useState(null);
    const [imagePath, setImagePath] = useState('');
    const [isActive, setIsActive] = useState(true);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const navigate = useNavigate();
    const apiUrl = process.env.REACT_APP_API_URL;

    const [kategoriOptions, setKategoriOptions] = useState([]);
const handleImageChange = (e) => {
  const file = e.target.files[0];

  if (file) {
    const reader = new FileReader();

    reader.onload = (event) => {
      const dataURL = event.target.result;
      // Mengonversi data URL menjadi byte dengan menghapus "data:image/jpeg;base64,"
      const bytes = dataURL.split(',')[1];

      // Simpan byte dalam state
      setImagePath(bytes);
    };

    // Membaca file sebagai data URL
    reader.readAsDataURL(file);
  }
};
    useEffect(() => {
      // Mengambil daftar kategori dari API
      axios
        .get(`${apiUrl}/api/Category`)
        .then((response) => {
          // Menyusun data menjadi bentuk objek { value, label } untuk dropdown
          const options = response.data.map((kategori) => ({
            value: kategori.id, // Sesuaikan dengan properti ID kategori di API
            label: kategori.categoryName, // Sesuaikan dengan properti nama kategori di API
          }));
          // Menyimpan opsi kategori ke dalam state
          setKategoriOptions(options);
        })
        .catch((error) => {
          console.error('Error fetching category options:', error);
        });

      // Mengambil data kursus berdasarkan ID dan mengisikan formulir dengan data tersebut
      axios
        .get(`${apiUrl}/api/Course/${id}`)
        .then((response) => {
          const courseData = response.data;
          setTitle(courseData.title);
          setDescription(courseData.deskripsi);
          setPrice(courseData.price);
          setSelectedKategori({
            value: courseData.categoryId,
            label: courseData.categoryName, // Sesuaikan dengan properti nama kategori di API
          });
          setImagePath(courseData.imagePath);
          setIsActive(courseData.isActive);
          console.log(response);
        })
        .catch((error) => {
          console.error('Error fetching course data:', error);
        });
    }, [id]);

    const handleSubmit = (e) => {
      e.preventDefault();
const parsedPrice = parseInt(price, 10);
      const payload = {
        title: title,
        description: description,
        price: parsedPrice,
        merkId: selectedKategori.value,
        imagePath: imagePath,
        isActive: isActive,
      };

      axios
        .put(`${apiUrl}/api/Course/${id}`, payload)
        .then((response) => {
          console.log('Course updated successfully:', response.data);
          setSnackbarOpen(true);

          setTimeout(() => {
            navigate('/admincourse');
          }, 2000);
        })
        .catch((error) => {
          console.error('Error updating course:', error);
        });
    };

    return (
      <Box p={3}>
        {' '}
        {/* Padding untuk memberikan jarak dari tepi konten */}
        <form autoComplete="off" onSubmit={handleSubmit}>
          <h2>Edit Course Saya</h2>
          <TextField
            label="Title"
            required
            variant="outlined"
            color="secondary"
            fullWidth
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            sx={{ mb: 2 }}
          />
          <TextField
            label="Description"
            required
            variant="outlined"
            color="secondary"
            fullWidth
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            sx={{ mb: 2 }}
          />
          <TextField
            label="Price"
            required
            variant="outlined"
            color="secondary"
            fullWidth
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            sx={{ mb: 2 }}
          />
          <Box sx={{ mt: 3 }}>
            <Box>
              <Box
                component="img"
                sx={{ height: '140px' }}
                src={`data:image/png;base64,${imagePath}`}
              />
            </Box>
            <TextField
              type="file"
              inputProps={{ accept: 'image/*' }}
              onChange={handleImageChange}
            />
          </Box>
          <div style={{ marginTop: '16px' }}>
            <Select
              label="Kategori Course"
              options={kategoriOptions}
              value={selectedKategori}
              onChange={(option) => setSelectedKategori(option)}
              placeholder="Pilih Kategori"
              sx={{ mb: 2 }}
            />
          </div>

          <div style={{ marginTop: '16px' }}>
            <RadioGroup
              row
              name="status"
              value={isActive}
              onChange={(e) => setIsActive(e.target.value === 'true')}
            >
              <FormControlLabel
                value="true"
                control={<Radio color="primary" />}
                label="Aktif"
              />
              <FormControlLabel
                value="false"
                control={<Radio color="primary" />}
                label="Tidak Aktif"
              />
            </RadioGroup>
          </div>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            style={{ marginTop: '16px' }}
          >
            Save
          </Button>
        </form>
        {/* Snackbar for notification */}
        <Snackbar
          open={snackbarOpen}
          autoHideDuration={6000}
          onClose={() => setSnackbarOpen(false)}
          message="Data berhasil diubah!"
          sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF', mt: 2 }}
        />
      </Box>
    );
  };

  export default EditCourseComponent;
