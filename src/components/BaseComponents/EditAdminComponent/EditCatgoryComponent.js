import React, { useState, useEffect } from 'react';
import {
  TextField,
  Button,
  Radio,
  RadioGroup,
  FormControlLabel,
  Snackbar,
  Box,
} from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';

const EditCategoryComponent = () => {
  const { id } = useParams(); // Dapatkan parameter 'id' dari URL
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [imagePath, setImagePath] = useState('');
  const [status, setStatus] = useState('true'); // Default status true
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const navigate = useNavigate(); // Gunakan useNavigate untuk navigasi
  const apiUrl = process.env.REACT_APP_API_URL;


  useEffect(() => {
    // Fetch category data using categoryId and populate the form fields
    axios
      .get(`${apiUrl}/api/Category/${id}`)
      .then((response) => {
        const categoryData = response.data;
        setName(categoryData.categoryName);
        setDescription(categoryData.deskripsi);
        setImagePath(categoryData.imagePath);
        setStatus(categoryData.isActive ? 'true' : 'false');
      })
      .catch((error) => {
        console.error('Error fetching category data:', error);
      });
  }, [id]);
console.log(name);
  const handleSubmit = (e) => {
    e.preventDefault();

    const payload = {
      categoryName: name,
      deskripsi: description,
      imagePath: imagePath,
      isActive: status === 'true',
    };

    console.log(payload);

    axios
      .put(`${apiUrl}/api/Category/${id}`, payload)
      .then((response) => {
        console.log('Category updated successfully:', response.data);
        setSnackbarOpen(true);

        setTimeout(() => {
          navigate('/admincategory');
        }, 2000);
      })
      .catch((error) => {
        console.error('Error updating category:', error);
      });
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (event) => {
        const dataURL = event.target.result;
        // Mengonversi data URL menjadi byte dengan menghapus "data:image/jpeg;base64,"
        const bytes = dataURL.split(',')[1];

        // Simpan byte dalam state
        setImagePath(bytes);
      };

      // Membaca file sebagai data URL
      reader.readAsDataURL(file);
    }
  };

  return (
    <div>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <h2>Edit Category</h2>
        <TextField
          label="Name"
          required
          variant="outlined"
          color="secondary"
          type="text"
          fullWidth
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <TextField
          label="Description"
          required
          variant="outlined"
          color="secondary"
          type="text"
          fullWidth
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <Box sx={{ mt: 3 }}>
          <Box>
            <Box
              component="img"
              sx={{ height: '140px' }}
              src={`data:image/png;base64,${imagePath}`}
            />
          </Box>
          <TextField
            type="file"
            inputProps={{ accept: 'image/*' }}
            onChange={handleImageChange}
          />
        </Box>
        {/* Pilihan status menggunakan tombol radio */}
        <div style={{ marginTop: '16px' }}>
          <RadioGroup
            row
            name="status"
            value={status}
            onChange={(e) => setStatus(e.target.value)}
          >
            <FormControlLabel
              value="true"
              control={<Radio color="primary" />}
              label="Aktif"
            />
            <FormControlLabel
              value="false"
              control={<Radio color="primary" />}
              label="Tidak Aktif"
            />
          </RadioGroup>
        </div>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          style={{ marginTop: '16px' }}
        >
          Save
        </Button>
      </form>

      {/* Snackbar for notification */}
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message="Data berhasil diubah!"
        sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF' }}
      />
    </div>
  );
};

export default EditCategoryComponent;


