import React, { useState, useEffect } from 'react';
import { TextField, Button, Snackbar, RadioGroup, FormControlLabel, Radio } from '@mui/material';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';

const EditUserComponent = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [isActivated, setisActivated] = useState(''); // Ganti dengan isActivated yang sesuai dari API
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const apiUrl = process.env.REACT_APP_API_URL;


  useEffect(() => {
    // Memuat data pengguna berdasarkan ID dari API
    axios
      .get(`${apiUrl}/api/User/${id}`)
      .then((response) => {
        const userData = response.data;
        setUsername(userData.username);
        setPassword(userData.password);
        setEmail(userData.email);
        setisActivated(userData.isActivated); // Sesuaikan dengan properti isActivated di API
      })
      .catch((error) => {
        console.log('Error Fetching User Data: ', error);
      });
  }, [id]);

  const handleSubmit = (e) => {
    e.preventDefault();

    const payload = {
      username: username,
      password: password,
      email: email,
      isActivated: isActivated, // Sesuaikan dengan properti isActivated di API
    };

    axios
      .put(`${apiUrl}/api/User/${id}`, payload)
      .then((response) => {
        console.log('User updated successfully:', response.data);
        setSnackbarOpen(true);

        setTimeout(() => {
          navigate('/adminuser');
        }, 2000);
      })
      .catch((error) => {
        console.error('Error updating user:', error);
      });
  };

  return (
    <div>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <h2>Edit User</h2>
        <TextField
          label="Username"
          required
          variant="outlined"
          color="secondary"
          fullWidth
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <TextField
          label="Password"
          required
          variant="outlined"
          color="secondary"
          type="password"
          fullWidth
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <TextField
          label="Email"
          required
          variant="outlined"
          color="secondary"
          fullWidth
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <div style={{ marginTop: '16px' }}>
          <RadioGroup
            row
            name="isActivated"
            value={isActivated}
            onChange={(e) => setisActivated(e.target.value)}
          >
            <FormControlLabel
              value="true"
              control={<Radio color="primary" />}
              label="Aktif"
            />
            <FormControlLabel
              value="false"
              control={<Radio color="primary" />}
              label="Tidak Aktif"
            />
          </RadioGroup>
        </div>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          style={{ marginTop: '16px' }}
        >
          Save
        </Button>
      </form>

      {/* Snackbar for notification */}
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message="Data berhasil diubah!"
        sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF' }}
      />
    </div>
  );
};

export default EditUserComponent;
