import React, { useEffect, useState } from 'react';
import {
  TextField,
  Button,
  Radio,
  RadioGroup,
  FormControlLabel,
  Snackbar,
  Box,
} from '@mui/material';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';

const EditPaymentComponent = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [logo, setLogo] = useState('');
  const [status, setStatus] = useState('true');
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const apiUrl = process.env.REACT_APP_API_URL;


  useEffect(() => {
    axios
      .get(`${apiUrl}/api/PaymentMethod/${id}`)
      .then((response) => {
        const paymentMethodData = response.data;
        setName(paymentMethodData.name);
        setLogo(paymentMethodData.logo);
        setStatus(paymentMethodData.isActive ? 'true' : 'false');
      })
      .catch((error) => {
        console.log('Error Fetching Method Data: ', error);
      });
  }, [id]);

  const handleSubmit = (e) => {
    e.preventDefault();

    const payload = {
      name: name,
      logo: logo,
      isActive: status === 'true',
    };

    axios
      .put(`${apiUrl}/api/PaymentMethod/${id}`, payload)
      .then((response) => {
        console.log('Payment Method updated successfully:', response.data);
        setSnackbarOpen(true);

        setTimeout(() => {
          navigate('/adminpayment');
        }, 2000);
      })
      .catch((error) => {
        console.error('Error updating payment method:', error);
      });
  };
  const handleImageChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (event) => {
        const dataURL = event.target.result;
        // Mengonversi data URL menjadi byte dengan menghapus "data:image/jpeg;base64,"
        const bytes = dataURL.split(',')[1];

        // Simpan byte dalam state
        setLogo(bytes);
      };

      // Membaca file sebagai data URL
      reader.readAsDataURL(file);
    }
  };

  return (
    <div>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <h2>Edit Payment Method</h2>
        <TextField
          label="Name"
          required
          variant="outlined"
          color="secondary"
          type="text"
          fullWidth
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <Box sx={{ mt: 3 }}>
          <Box>
            <Box
              component="img"
              sx={{ height: '140px' }}
              src={`data:image/png;base64,${logo}`}
            />
          </Box>
          <TextField
            type="file"
            inputProps={{ accept: 'image/*' }}
            onChange={handleImageChange}
          />
        </Box>
        {/* Pilihan status menggunakan tombol radio */}
        <div style={{ marginTop: '16px' }}>
          <RadioGroup
            row
            name="status"
            value={status}
            onChange={(e) => setStatus(e.target.value)}
          >
            <FormControlLabel
              value="true"
              control={<Radio color="primary" />}
              label="Aktif"
            />
            <FormControlLabel
              value="false"
              control={<Radio color="primary" />}
              label="Tidak Aktif"
            />
          </RadioGroup>
        </div>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          style={{ marginTop: '16px' }}
        >
          Save
        </Button>
      </form>

      {/* Snackbar for notification */}
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={() => setSnackbarOpen(false)}
        message="Data berhasil diubah!"
        sx={{ backgroundColor: '#4CAF50', color: '#FFFFFF' }}
      />
    </div>
  );
};

export default EditPaymentComponent;
