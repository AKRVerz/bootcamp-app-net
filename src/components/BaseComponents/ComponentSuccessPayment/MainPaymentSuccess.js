import React, { useState, useEffect } from 'react';
import Box from '@mui/system/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useMediaQuery } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import axios from 'axios';

const MainPaymentSuccess = () => {

  const isMobile = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const navigate = useNavigate();

  const goToLoginPage = () => {
    navigate('/login');
  };

  const goToInvoicePage = () => {
    navigate('/invoice');
  };

  return (
    <Box
      sx={{
        width: isMobile ? '90%' : '616px',
        height: 'auto',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        padding: isMobile ? '20px 10px' : '20px',
        border: 'none',
        boxShadow: 'none',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
      }}
    >
      {/* Konten lainnya */}
      <Typography
        variant="h2"
        sx={{
          color: '#790B0A',
          fontSize: isMobile ? '20px' : '24px',
          margin: '0 0 16px 0',
        }}
      >
        Purchase Successfully
      </Typography>
      {/* Konten lainnya */}
      <Button
        variant="outlined"
        color="error"
        onClick={goToLoginPage}
        startIcon={<HomeIcon />}
        sx={{
          width: '200px',
          height: '38px',
          fontWeight: 'bold',
          borderRadius: '8px',
          padding: '10px',
          backgroundColor: '#fffff',
          color: 'red',
          '&:hover': {
            backgroundColor: '#e5bebe',
          },
        }}
      >
        Back to Home
      </Button>

      <Button
        variant="contained"
        color="primary"
        onClick={goToInvoicePage}
        endIcon={<ArrowForwardIcon />}
        sx={{
          width: '200px',
          height: '38px',
          borderRadius: '8px',
          padding: '10px',
          backgroundColor: '#790B0A',
          color: 'white',
          '&:hover': {
            backgroundColor: '#8E0C10',
          },
        }}
      >
        Open Invoice
      </Button>
    </Box>
  );
};

export default MainPaymentSuccess;
