import React from 'react';
import useTheme from '../../hooks/useTheme';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';

export default function ToggleTheme() {
  const { currentTheme, toggleTheme } = useTheme();

  const handleToggleTheme = () => {
    toggleTheme();
    document.documentElement.classList.add('theme-transition');
    setTimeout(() => {
      document.documentElement.classList.remove('theme-transition');
    }, 1000);
  };

  const buttonStyle = {
    color: '#000',
    backgroundColor: '#fff',
  };

  return (
    <Tooltip
      title={`Switch to ${currentTheme === 'light' ? 'Dark' : 'Light'} Mode`}
      arrow
      TransitionComponent={Zoom}
      placement="bottom"
    >
      <IconButton
        color="inherit"
        aria-label="Toggle Theme"
        onClick={handleToggleTheme}
        style={buttonStyle}
      >
        {currentTheme === 'light' ? (
          <Brightness4Icon fontSize="small" />
        ) : (
          <Brightness7Icon fontSize="small" />
        )}
      </IconButton>
    </Tooltip>
  );
}
