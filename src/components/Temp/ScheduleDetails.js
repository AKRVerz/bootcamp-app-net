import React from 'react';
import useScheduleById from '../../hooks/useScheduleById';

function ScheduleDetails({ id }) {
  const { schedule, course, loading, error } = useScheduleById(id);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div>
      <h2>Schedule Details</h2>
      <p>Schedule Name: {schedule.name}</p>
      <p>Course Name: {course.name}</p>
      {/* Tampilkan informasi lainnya tentang schedule dan course di sini */}
    </div>
  );
}

export default ScheduleDetails;
