import useCart from '../../hooks/useCart';

export default function CartData() {
  const cartContext = useCart();

  return <div color="red">Cart {cartContext.totalCart}</div>;
}
