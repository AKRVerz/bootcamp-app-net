import React from 'react';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

const ErrorModal = ({ open, onClose, message }) => {
  return (
    <Modal open={open} onClose={onClose}>
      <Box
        sx={{
          position: 'absolute',
          width: 300,
          backgroundColor: 'white',
          padding: 2,
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          boxShadow: 24,
          borderRadius: 4,
          textAlign: 'center',
        }}
      >
        <Typography variant="h6">Error</Typography>
        <Typography variant="body1">{message}</Typography>
      </Box>
    </Modal>
  );
};

export default ErrorModal;
