import React, { useContext, useEffect, useState } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from 'react-router-dom';
import Home from './components/Pages/Home/Home';
import LoginPage from './components/Pages/Login/LoginPage';
import RegisterPage from './components/Pages/Register/RegisterPage';
import ResetPasswordPage from './components/Pages/Reset_Password/ResetPasswordPage';
import NewPasswordPage from './components/Pages/New_Password/NewPasswordPage';
import ConfirmEmailPage from './components/Pages/ConfirmEmail/ConfirmEmailPage';
import MyClassPage from './components/Pages/MyClass/MyClassPage';
import MenuCourse from './components/Pages/Menu_Course/MenuCourse';
import { ThemeProvider, createTheme } from '@mui/material';
import useTheme from './hooks/useTheme';
import CheckOutPage from './components/Pages/Checkout/CheckOutPage';
import SuccessPaymentPage from './components/Pages/SuccessPayment/SuccessPaymentPage';
import InvoicePage from './components/Pages/Invoice/InvoicePage';
import InvoiceDetailPage from './components/Pages/Invoice/InvoiceDetailPage';
import AdminCoursePage from './components/Pages/AdminCourse/AdminCoursePage';
import AdminCategoryPage from './components/Pages/AdminCategory/AdminCategoryPage';
import AdminPaymentPage from './components/Pages/AdminPayment/AdminPaymentPage';
import AdminUserPage from './components/Pages/AdminUser/AdminUserPage';
import EditCategoryPage from './components/Pages/AdminEdit/EditCategoryPage';
import EditCoursePage from './components/Pages/AdminEdit/EditCoursePage';
import EditPaymentPage from './components/Pages/AdminEdit/EditPaymentPage';
import EditUserPage from './components/Pages/AdminEdit/EditUserPage';
import AdminInvoicePage from './components/Pages/AdminInvoice/AdminInvoicePage';
import CreateCoursePage from './components/Pages/AdminCreate/CreateCoursePage';
import CreatePaymentPage from './components/Pages/AdminCreate/CreatePaymentPage';
import CreateUserPage from './components/Pages/AdminCreate/CreateUserPage';
import ClassUserPage from './components/Pages/MyClass/ClassUserPage';
import { AuthContext } from './contexts/AuthContext';
import useAuth from './hooks/useAuth';
import jwt from 'jwt-decode';
import CreateCategoryPage from './components/Pages/AdminCreate/CreateCategoryPage';
import AdminSchedulePage from './components/Pages/AdminSchedule/AdminSchedulePage';
import CreateSchedulePage from './components/Pages/AdminCreate/CreateSchedulePage';
import EditSchedulePage from './components/Pages/AdminEdit/EditSchedulePage';
import NotFoundPage from './components/Pages/404';
const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const authContext = useContext(AuthContext);

  const { payload } = useAuth();
  const [role, setRole] = useState('');

  useEffect(() => {
    if (payload) {
      const token = jwt(payload.token);
      setRole(token.role);
    }
  }, []);

  useEffect(() => {
    setIsLoggedIn(authContext.isLoggedIn);
  }, [authContext]);

  const { currentTheme } = useTheme();

  const mainTheme = createTheme({
    palette: {
      mode: currentTheme,
    },
    typography: {
      fontFamily: ['Montserrat', 'sans-serif'].join(','),
    },
  });

  return (
    <ThemeProvider theme={mainTheme}>
      <Router>
        <div>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
            <Route path="/forgot-password" element={<ResetPasswordPage />} />
            <Route path="/reset-password" element={<NewPasswordPage />} />
            <Route path="/confirm-email" element={<ConfirmEmailPage />} />
            <Route
              path="/class-course"
              element={isLoggedIn ? <ClassUserPage /> : <LoginPage />}
            />
            <Route
              path="/myclass/:id"
              element={isLoggedIn ? <MyClassPage /> : <LoginPage />}
            />
            <Route
              path="/menu-course/:id"
              element={isLoggedIn ? <MenuCourse /> : <LoginPage />}
            />
            <Route
              path="/checkout"
              element={isLoggedIn ? <CheckOutPage /> : <LoginPage />}
            />
            <Route
              path="/success-payment"
              element={isLoggedIn ? <SuccessPaymentPage /> : <LoginPage />}
            />
            <Route
              path="/invoice"
              element={isLoggedIn ? <InvoicePage /> : <LoginPage />}
            />
            <Route
              path="/invoice-detail/:id"
              element={isLoggedIn ? <InvoiceDetailPage /> : <LoginPage />}
            />
            {role === 'admin' && (
              <Route
                path="/adminCourse"
                element={isLoggedIn ? <AdminCoursePage /> : <LoginPage />}
              >
                <Route
                  path="/adminCategory"
                  element={isLoggedIn ? <AdminCategoryPage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminPayment"
                  element={isLoggedIn ? <AdminPaymentPage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminUser"
                  element={isLoggedIn ? <AdminUserPage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminSchedule"
                  element={isLoggedIn ? <AdminSchedulePage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminEditCategory/:id"
                  element={isLoggedIn ? <EditCategoryPage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminEditCourse/:id"
                  element={isLoggedIn ? <EditCoursePage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminEditPayment/:id"
                  element={isLoggedIn ? <EditPaymentPage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminEditUser/:id"
                  element={isLoggedIn ? <EditUserPage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminEditSchedule/:id"
                  element={isLoggedIn ? <EditSchedulePage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminInvoice"
                  element={isLoggedIn ? <AdminInvoicePage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminCreateCourse"
                  element={isLoggedIn ? <CreateCoursePage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminCreateSchedule"
                  element={isLoggedIn ? <CreateSchedulePage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminCreateCategory"
                  element={isLoggedIn ? <CreateCategoryPage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminCreatePayment"
                  element={isLoggedIn ? <CreatePaymentPage /> : <LoginPage />}
                ></Route>
                <Route
                  path="/adminCreateUser"
                  element={isLoggedIn ? <CreateUserPage /> : <LoginPage />}
                ></Route>
              </Route>
            )}
          </Routes>
        </div>
      </Router>
    </ThemeProvider>
  );
};

export default App;
