import { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

const useCarsDataIsActive = (isActive) => {
  const [carsData, setCarsData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const apiUrl = process.env.REACT_APP_API_URL;
        const response = await axios.get(
          `${apiUrl}/api/Course/Active`
        );
        setCarsData(response.data);
        setLoading(false);
      } catch (error) {
        if (error.response) {
          setError('Error response from server: ' + error.response.data);
        } else if (error.request) {
          setError('No response from server: ' + error.request);
        } else {
          setError('Error sending request: ' + error.message);
        }
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return { carsData, loading, error };
};

useCarsDataIsActive.propTypes = {
  isActive: PropTypes.bool.isRequired,
  carsData: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      schedule: PropTypes.string.isRequired,
      merkId: PropTypes.string.isRequired,
      imagePath: PropTypes.string.isRequired,
      created: PropTypes.string.isRequired,
      updated: PropTypes.string.isRequired,
      categoryName: PropTypes.string.isRequired,
    })
  ),
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

export default useCarsDataIsActive;
