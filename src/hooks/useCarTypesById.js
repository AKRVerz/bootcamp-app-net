import { useState, useEffect } from 'react';
import axios from 'axios';

const useCarTypesById = (id) => {
  const [carTypes, setCarTypes] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const apiUrl = process.env.REACT_APP_API_URL;
        const response = await axios.get(`${apiUrl}/api/Category`);
        setCarTypes(response.data);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  // Tambahkan kode untuk mencari mobil berdasarkan 'id' di sini
  const car = carTypes.find((carType) => carType.id === id);

  return { car, loading, error };
};

export default useCarTypesById;
