// hooks/useCart.js
import { useState } from 'react';

const useCart = () => {
  const [cartItems, setCartItems] = useState([]);
  const [schedules, setSchedules] = useState([]); // Tambahkan schedules

  const addToCart = (item) => {
    setCartItems([...cartItems, item]);
  };

  const addSchedule = (schedule) => {
    setSchedules([...schedules, schedule]);
  };

  return {
    cartItems,
    addToCart,
    schedules, // Sertakan schedules dalam objek yang dikembalikan
    addSchedule, // Tambahkan fungsi addSchedule
  };
};

export default useCart;
