import Cookies from 'js-cookie';
import { useNavigate } from 'react-router-dom';

export const useCustomNavigate = () => {
  const navigate = useNavigate();
  const goToAdminCourse = () => navigate('/adminCourse');
  const goToAdminCategoryCourse = () => navigate('/adminCategory');
  const goToAdminPayment = () => navigate('/adminPayment');
  const goToAdminUsers = () => navigate('/adminUser');
  const goToAdminInvoice = () => navigate('/adminInvoice');
  const goToAdminSchedule = () => navigate('/adminSchedule')
const logOut = () => { Cookies.remove('payload');navigate('/')};

  return {
    goToAdminCourse,
    goToAdminCategoryCourse,
    goToAdminPayment,
    goToAdminUsers,
    goToAdminInvoice,
    goToAdminSchedule,
    logOut,
  };
};
