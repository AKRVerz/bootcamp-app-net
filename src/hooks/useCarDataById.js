import { useState, useEffect } from 'react';
import axios from 'axios';

const useCarsDataById = (id) => {
  const [carsData, setCarsData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const apiUrl = process.env.REACT_APP_API_URL;
        const carsResponse = await axios.get(`${apiUrl}/api/Course`);
        const categoriesResponse = await axios.get(`${apiUrl}/api/Category`);
        console.log(carsResponse.data);

        const carsWithCategories = carsResponse.data.map((car) => {
          const category = categoriesResponse.data.find(
            (category) => category.id === car.merkId
          );
          if (category) {
            return {
              ...car,
              categoryName: category.categoryName,
            };
          }
          return car;
        });

        setCarsData(carsWithCategories);
        setLoading(false);
      } catch (error) {
        if (error.response) {
          setError('Error response from server: ' + error.response.data);
        } else if (error.request) {
          setError('No response from server: ' + error.request);
        } else {
          setError('Error sending request: ' + error.message);
        }
        setLoading(false);
      }
    };

    fetchData();
  }, []);
  const car = carsData.find((carsData) => carsData.id === id);
  return { car, loading, error };
};

export default useCarsDataById;
