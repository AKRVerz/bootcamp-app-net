import { useState } from 'react';
import axios from 'axios';

const useNewPassword = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(false);
  const apiUrl = process.env.REACT_APP_API_URL;


  const resetPassword = async (password, confirmPassword) => {
    setIsLoading(true);
    setError(null);
    setSuccess(false);

    try {
      const searchParams = new URLSearchParams(window.location.search);
      const email = searchParams.get('email');

      if (!email) {
        throw new Error('Email not found in URL');
      }

      const response = await axios.post(
        `${apiUrl}/api/User/ResetPassword`,
        { email, password, confirmPassword },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );

      if (response.status === 200) {
        setSuccess(true);
      } else {
        throw new Error('Password reset failed');
      }
    } catch (err) {
      setError(err.message);
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };

  return { isLoading, success, error, resetPassword };
};

export default useNewPassword;
