import { useState, useEffect } from 'react';
import axios from 'axios';

const useScheduleById = (courseId) => {
  const [scheduleData, setScheduleData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const url = process.env.REACT_APP_API_URL;


  useEffect(() => {
    const fetchData = async () => {
      try {
        const apiUrl = `${url}/api/Schedule`; // Ganti dengan URL API yang sesuai
        const response = await axios.get(apiUrl);
        const schedules = response.data.filter(
          (item) => item.courseFK === courseId
        );

        if (schedules.length > 0) {
          setScheduleData(schedules);
        } else {
          setError('Jadwal tidak ditemukan untuk course ini.');
        }

        setLoading(false);
      } catch (error) {
        setError('Terjadi kesalahan saat mengambil data jadwal.');
        setLoading(false);
      }
    };

    fetchData();
  }, [courseId]);

  return { scheduleData, loading, error };
};

export default useScheduleById;
