import { useEffect, useState } from 'react';
import axios from 'axios'; // Impor Axios

const useUserDataByUsername = (username) => {
  const [userId, setUserId] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const apiUrl = process.env.REACT_APP_API_URL;


  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `${apiUrl}/api/User/GetByUsername/${username}`
        );

        const userData = response.data;
        setUserId(userData.userId);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, [username]);

  return { userId, loading, error };
};

export default useUserDataByUsername;
