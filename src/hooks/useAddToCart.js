import { useState } from 'react';
import axios from 'axios'; // Import Axios

const useAddToCart = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const Url = process.env.REACT_APP_API_URL;


  const addToCart = async (userId, selectedSchedule) => {
    // Pass userId as a parameter
    setLoading(true);

    try {
      const apiUrl = `${Url}/api/Cart`;
      const response = await axios.post(apiUrl, {
        userId, // Use the passed userId parameter
        scheduleId: selectedSchedule,
      });

      if (response.status === 201) {
        // Check for the correct status code for a successful creation
        console.log('Cart berhasil ditambahkan');
        // Other success actions as needed
      } else {
        console.error('Gagal menambahkan ke Cart');
        // Handle errors if needed
        setError('Gagal menambahkan ke Cart');
      }
    } catch (error) {
      console.error('Terjadi kesalahan:', error);
      setError('Terjadi kesalahan');
    } finally {
      setLoading(false);
    }
  };

  return { addToCart, loading, error };
};

export default useAddToCart;
